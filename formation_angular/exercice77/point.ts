export class Point {
 private _abs : number;
 private _ord : number;

 constructor(_abs: number,
     _ord: number){
         this.abs= _abs;
         this.ord= _ord;
     }

    public get abs(): number {
        return this._abs;
    }

    public set abs(_abs: number) {
        this._abs = _abs;
    }

    public get ord(): number {
        return this._ord;
    }

    public set ord(_ord: number) {
        this._ord = _ord;
    }

    public calculerDistance(p: Point):number{
        let res=0;
        res = Math.sqrt( Math.pow( (this.abs - p.abs),2 ) 
        + Math.pow( (this.ord - p.ord),2 ) )
        return res;
    }

    public calculerMilieu(p: Point): Point{
       let res : Point = new Point((this.abs + p.abs)/2,(this.ord + p.ord)/2);
        return res;
    }

}