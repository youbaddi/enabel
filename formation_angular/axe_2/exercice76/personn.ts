export class Personne {
    private _num: number;
    private _nom: string;
    private _prenom: string;

    public constructor(_num: number, _nom: string, _prenom:
        string) {
        this.num = _num;
        this._nom = _nom;
        this._prenom = _prenom;
    }

    public set num(_num: number) {
        this._num = (_num >= 0 ? _num : 0);
    }
    public set nom(_nom: string) {
        this._nom = _nom;
    }
    public set prenom(_prenom: string) {
        this._prenom = _prenom;
    }

    public get num() {
        return this._num ;
    }
    public get nom() {
        return this._nom;
    }
    public get prenom() {
       return  this._prenom ;
    }
}