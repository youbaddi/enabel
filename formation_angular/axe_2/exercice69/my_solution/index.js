var http = require('http');
var url = require('url');
var querystring = require('querystring');

var server = http.createServer(function(req, res) {
    var params = querystring.parse(url.parse(req.url).
     query);
    res.writeHead(200, {"Content-Type": "text/plain"});
    if ('operand1' in params && 'operand2' in params) {
        var resultat = eval(parseInt(params['operand1']) + parseInt(params['operand2']));
        res.write('la somme de ' + params['operand1'] + ' et ' + 
        params['operand2'] +' est ' +
        resultat);
}
else {
        res.write('Vous devez bien avoir un operand1 et un operand2, non ?');
}
    res.end();
});
server.listen(8080);
 