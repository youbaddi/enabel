var express = require('express');
var app = express();
var middleWare1 = function (req, res, next) {
  console.log("middleWare1:", req.url);
next(); };
var middleWare2 = function (req, res, next) {
  console.log("middleWare2:", req.url);
};
app.get('/person/:nom', function (req, res,next) {
  console.log("requete recu");
  res.render('index.ejs', {nom : res.params.nom});
  next();
}, middleWare1, middleWare2
);
app.listen(8080, function () {
  console.log("Express en attente");
});