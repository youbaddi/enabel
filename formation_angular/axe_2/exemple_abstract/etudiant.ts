import { Personne } from "./personn";
import { f } from "./decorators";

export class Etudiant extends Personne {
    
    
    private _niveau: number;
    

    public constructor(_num: number, _nom: string, 
        _prenom: string, _niveau: number) {
        super(_num, _nom, _prenom);
        this._niveau = _niveau;
   
    }

    public set niveau(_niveau: number) {
        this._niveau = _niveau;
    }

    public get niveau() {
       return    this._niveau ;
    }

    afficherDetails(): void {
        console.log(this);
    }
    
    @f()
    afficherNom(): void {
        console.log(this._nom);
    }
    
}