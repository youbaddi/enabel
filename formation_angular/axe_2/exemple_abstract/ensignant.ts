import { Personne } from "./personn";

export class Ensignant extends Personne {
    private _salaire: number;

       

    public constructor(_num: number, _nom: string, 
        _prenom: string, _salaire: number) {
        super(_num, _nom, _prenom);
        this._salaire = _salaire;
   
    }

    public get salaire(): number {
        return this._salaire;
    }

    public set salaire(_salaire: number) {
        this._salaire = _salaire;
    }

    afficherDetails(): void {
        console.log(this);
    }

    afficherNom(): void {
        console.log(this._nom);
    }
    
}