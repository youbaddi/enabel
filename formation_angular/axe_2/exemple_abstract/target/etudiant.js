"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Etudiant = void 0;
var personn_1 = require("./personn");
var decorators_1 = require("../decorators");
var Etudiant = /** @class */ (function (_super) {
    __extends(Etudiant, _super);
    function Etudiant(_num, _nom, _prenom, _niveau) {
        var _this = _super.call(this, _num, _nom, _prenom) || this;
        _this._niveau = _niveau;
        return _this;
    }
    Object.defineProperty(Etudiant.prototype, "niveau", {
        get: function () {
            return this._niveau;
        },
        set: function (_niveau) {
            this._niveau = _niveau;
        },
        enumerable: false,
        configurable: true
    });
    Etudiant.prototype.afficherDetails = function () {
        console.log(this);
    };
    Etudiant.prototype.afficherNom = function () {
        console.log(this._nom);
    };
    __decorate([
        decorators_1.f()
    ], Etudiant.prototype, "afficherNom", null);
    return Etudiant;
}(personn_1.Personne));
exports.Etudiant = Etudiant;
