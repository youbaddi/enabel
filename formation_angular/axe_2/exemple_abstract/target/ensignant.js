"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.Ensignant = void 0;
var personn_1 = require("./personn");
var Ensignant = /** @class */ (function (_super) {
    __extends(Ensignant, _super);
    function Ensignant(_num, _nom, _prenom, _salaire) {
        var _this = _super.call(this, _num, _nom, _prenom) || this;
        _this._salaire = _salaire;
        return _this;
    }
    Object.defineProperty(Ensignant.prototype, "salaire", {
        get: function () {
            return this._salaire;
        },
        set: function (_salaire) {
            this._salaire = _salaire;
        },
        enumerable: false,
        configurable: true
    });
    Ensignant.prototype.afficherDetails = function () {
        console.log(this);
    };
    Ensignant.prototype.afficherNom = function () {
        console.log(this._nom);
    };
    return Ensignant;
}(personn_1.Personne));
exports.Ensignant = Ensignant;
