"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var etudiant_1 = require("./etudiant");
var ensignant_1 = require("./ensignant");
console.log("hello world");
var etudiant = new etudiant_1.Etudiant(1, "baddi", "youssef", 1);
etudiant.afficherNom();
var enseignant = new ensignant_1.Ensignant(1, "adil", "El amri", 1);
var personnes = [
    enseignant, etudiant
];
for (var _i = 0, personnes_1 = personnes; _i < personnes_1.length; _i++) {
    var p = personnes_1[_i];
    console.log(p);
    if (p instanceof ensignant_1.Ensignant)
        console.log(p.salaire);
    else if (p instanceof etudiant_1.Etudiant)
        console.log(p.niveau);
    else
        console.log(p.num);
}
