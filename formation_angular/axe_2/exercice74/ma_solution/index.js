var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use('/assets',express.static('public'));

var mysql = require('mysql');

// get mysql connection
var myConnection  = require('express-myconnection')

var config = require('./config')
var params = {
    host:      config.database.host,
    user:       config.database.user,
    password: config.database.password,
    port:       config.database.port,
    database: config.database.db
}
app.use(myConnection(mysql, params, 'pool'))


  


app.set('engine view', 'ejs')
app.use(bodyParser.urlencoded({extended:false}))

app.get('/produit', function (req, res,next) {
    console.log("requete recu");
    var produits=[]
    // get product
    req.getConnection(function(error, conn) {
      conn.query('Select * from produit', function(
        err, rows) {
         if (err) throw err;
         produits = rows;
         for(i=0; i< rows.length; i++)          
            {console.log(rows[i].id + " " + rows[i].nom);
            produits[i] = rows[i];}
            console.log(produits);
            
            res.render('produit.ejs', {produits: produits });
            next();
            
       });
    
      }); 
       
    }
  );


app.get('/produit/create', function (req, res,next) {
  console.log("requete recu");
  res.render('produitCreate.ejs', {error:false});
  next();
  }
);

app.post('/produit/save', function (req, res,next) {
    console.log("requete recu");
    // get produit information
     var nom = req.body.nom;
     var prix = req.body.prix;
     var stock = req.body.stock;
     console.log(nom);
     console.log(prix);
     console.log(stock);
    
    // save pruit in database
    var data = {
        nom    : nom,
        prix : prix,
        stock   : stock
    };
    connection.connect();
    connection.query("Insert into produit set ? ",data,
      function(err, rows){
         
    if (err) { 
        res.render('produitCreate.ejs', {error:true});
        next(); throw err;
        }
    console.log("Insertion reussie");
    connection.end();
    
     // redirection to /produit
     res.redirect(200,'/produit');
     next();   
     
    });
    
    }
  );



  app.listen(config.server.port, function () {
  console.log("Express en attente");
});