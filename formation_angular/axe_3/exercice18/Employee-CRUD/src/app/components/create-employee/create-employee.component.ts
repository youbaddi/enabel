import { Component, OnInit } from '@angular/core';
import {Employee} from "../../models/employee";
import {EmployeeService} from "../../services/employee.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {

  employee: Employee = new Employee();
  submited:boolean=false;
  constructor(private es: EmployeeService,
              private router: Router) { }

  ngOnInit(): void {
  }


  onSubmit(){
this.submited = true;
this.save()
  }

  save(){
    // createEmployer
    this.es.createEmployer(this.employee)
      .subscribe((data) => console.log(data), error => console.log(error) );
    this.employee = new Employee();
    //this.gotolist();
  }

  gotolist(){
    this.router.navigate(['/employees']);
  }




}
