import { Component, OnInit } from '@angular/core';
import {EmployeeService} from "../../services/employee.service";
import {Observable} from "rxjs";
import {Employee} from "../../models/employee";
import {Router} from "@angular/router";

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  employees : Observable<Employee[]>
  id:number;
  employee: Employee;

  constructor(private es: EmployeeService,
              private router: Router) { }

  ngOnInit(): void {
    this.reloadData();
  }

  reloadData(){
    this.employees = this.es.getEmplyeesList();
  }

  deleteEmployee(id:number){
    // call the service that will call the backend
    this.es.deleteEmployee(id).subscribe( (data =>{
      console.log(data);
      this.reloadData();
      }
    ));
  }

  employeeDetails(id:number){
    // call the service that will call the backend
    this.router.navigate(['details',id]);
  }

  displayEmployeeDetails(id:number): void {
    this.employee = new Employee();
    this.id=id;
    console.log(this.id);
    this.employee.id=this.id;

    this.es.getEmplyee(this.id).subscribe( data => {
      console.log(data);
      this.employee = data;
    }, error => console.log(error))
  }

}
