import { Component, OnInit } from '@angular/core';
import {EmployeeService} from "../../services/employee.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Employee} from "../../models/employee";

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {

  id:number;
  employee: Employee;
  constructor(private es: EmployeeService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.employee = new Employee();
    this.id = this.route.snapshot.params['id'];
    console.log(this.id);
    this.employee.id=this.id;

    this.es.getEmplyee(this.id).subscribe( data => {
      console.log(data);
      this.employee = data;
    }, error => console.log(error))
  }

  list(){
    this.router.navigate(['employees']);
  }

}
