import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Employee} from "../models/employee";

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
 private baseUrl = 'http://localhost:8080/api/v1/employees'
  constructor(private http:HttpClient) { }

  getEmplyeesList(): Observable<any>{
   return this.http.get<any>(`${this.baseUrl}`);
  }

  getEmplyee(id: number):Observable<Employee> {
    return this.http.get<Employee>(`${this.baseUrl}/${id}`);
  }


  deleteEmployee(id: number):Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}` , {responseType : 'text'});
  }

  createEmployer(employee: Employee):Observable<any> {
    return this.http.post(`${this.baseUrl}` , employee);
  }



}
