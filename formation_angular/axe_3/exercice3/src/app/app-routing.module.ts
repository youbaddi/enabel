import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PokemonComponent} from "./composants/pokemon/pokemon.component";
import {PokemonsComponent} from "./composants/pokemons/pokemons.component";


const routes: Routes = [
  { path: '', component: PokemonsComponent },
  { path: 'pokemons', component: PokemonsComponent },
  { path: 'pokemon/:id', component: PokemonComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
