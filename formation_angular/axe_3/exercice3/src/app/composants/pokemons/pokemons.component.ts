import { Component, OnInit } from '@angular/core';
import {Pokemons} from "../../classes/pokemons";

@Component({
  selector: 'app-pokemons',
  templateUrl: './pokemons.component.html',
  styleUrls: ['./pokemons.component.css']
})
export class PokemonsComponent implements OnInit {

  pokemons:Pokemons;
  constructor() {
    this.pokemons  = new Pokemons();
  }

  ngOnInit(): void {
  }

}
