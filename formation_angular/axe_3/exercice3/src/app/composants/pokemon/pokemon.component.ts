import { Component, OnInit } from '@angular/core';
import {Pokemon} from "../../classes/pokemon";
import {ActivatedRoute} from "@angular/router";
import {Pokemons} from "../../classes/pokemons";

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.css']
})
export class PokemonComponent implements OnInit {
  pokemon : Pokemon;

  constructor(private route: ActivatedRoute) {
    //this.pokemon = new Pokemon(1,12,13, 'pickatsho', 'logo',['feu','electric'],new Date());
  }

  ngOnInit(): void {
     let id = this.route.paramMap.subscribe( res => {
       let id = res.get('id');
       console.log(id);
       let pokemons:Pokemons = new Pokemons();
       this.pokemon = pokemons.getPokemon(parseInt(id));
       console.log(this.pokemon);
     })


  }

}
