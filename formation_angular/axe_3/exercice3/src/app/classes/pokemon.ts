export class Pokemon {
  private _id: number;
  private _hp: number;
  private _cp: number;
  private _name: string;
  private _picture: string;
  private _types: string[];
  private _created: Date;


  constructor(id: number, hp: number, cp: number, name: string, picture: string, types: string[], created: Date) {
    this._id = id;
    this._hp = hp;
    this._cp = cp;
    this._name = name;
    this._picture = picture;
    this._types = types;
    this._created = created;
  }


  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get hp(): number {
    return this._hp;
  }

  set hp(value: number) {
    this._hp = value;
  }

  get cp(): number {
    return this._cp;
  }

  set cp(value: number) {
    this._cp = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get picture(): string {
    return this._picture;
  }

  set picture(value: string) {
    this._picture = value;
  }

  get types(): string[] {
    return this._types;
  }

  set types(value: string[]) {
    this._types = value;
  }

  get created(): Date {
    return this._created;
  }

  set created(value: Date) {
    this._created = value;
  }
}
