import {Pokemon} from "./pokemon";
import {pokemons} from "../config/mock-pokemons"

export class Pokemons {
  private  _items:Pokemon[] = [];


  constructor() {
    console.log(pokemons);
    for(let elt of pokemons){
      console.log(elt);
      console.log(elt.id);
      let pokemon: Pokemon  = new Pokemon(elt.id,elt.hp,
        elt.cp, elt.name, elt.picture, elt.types,elt.created);
       this.items.push(pokemon);
     }
    //this.items = pokemons

  }


  get items(): Pokemon[] {
    return this._items;
  }

  set items(value: Pokemon[]) {
    this._items = value;
  }

  getPokemon(id:number):Pokemon{
    console.log(id);
    console.log(this.items);
    console.log(this.items.filter(elt=> elt.id==id));
    return this.items.filter(elt=> elt.id==id)[0];
  }
}
