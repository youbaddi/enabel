import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MenuComponent} from "./composants/menu/menu.component";
import {AdresseComponent} from "./composants/adresse/adresse.component";
import {StagiaireComponent} from "./composants/stagiaire/stagiaire.component";


const routes: Routes = [
  { path: 'stagiaire', component: StagiaireComponent },
  { path: 'adresse', component: AdresseComponent },
  // { path: 'menu', component: MenuComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
