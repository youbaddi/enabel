import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Conponent1Component } from './conponent1.component';

describe('Conponent1Component', () => {
  let component: Conponent1Component;
  let fixture: ComponentFixture<Conponent1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Conponent1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Conponent1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
