import { Component } from '@angular/core';

import { Person } from './classes/person';
import { Personne } from './classes/personne'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'firstProjet';
  name ='baddi';
  age =18;
  person: Person = new Person(1,'baddi','youssef');

  personne: Personne = new Personne(100, 'Wick', 'John');

  type='number';

  tab = [4,2,3,6,9];

  dirBonjour(){
    return "hello world";
  }

  dirBonjour2(name:string){
    return "hello " + name;
  }
}
