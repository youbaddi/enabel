import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators, FormControl, FormGroup, FormArray} from "@angular/forms";
import {Personne} from "../../interfaces/personne";
import {PersonneService} from "../../services/personne.service";

@Component({
  selector: 'app-reactiveform',
  templateUrl: './reactiveform.component.html',
  styleUrls: ['./reactiveform.component.css']
})
export class ReactiveformComponent implements OnInit {

  personnes: Array <Personne> = new Array <Personne>();

  personneForm = this.fb.group({
    id: [null],
    nom: ['doe'],
    prenom: ['', [Validators.required, Validators.minLength(2)]],
    adresse: this.fb.group({
      rue: [''],
      ville: [''],
      codePostal: ['']
    }),
      sports: this.fb.array([
                              this.fb.control('')
                            ]),
  });

  constructor(private fb: FormBuilder,
              private personService: PersonneService) {
  }

  get sports() {
    return this.personneForm.get('sports') as FormArray;
  }

  ngOnInit() {
    this.updatePerson();

  }

  updatePerson(){
    this.personnes=this.personService.getAll();
  }

  ajouterSport() {
    this.sports.push(this.fb.control(''));
  }

  deleteSport(index:number) {
    this.sports.controls.splice(index,1);
  }

  afficherTout() {
    console.log(this.personneForm.value);
    this.personService.addPerson({ ...this.personneForm.value });
    this.updatePerson();
    console.log(this.personnes);
  }

  deletePersonne(id:number){
    // let index = this.personnes.map(item => item.id).indexOf(id);
    // this.personnes.splice(index,1);
    this.personnes = this.personnes.filter(item => item.id!=id);
  }
}
