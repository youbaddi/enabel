import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  text:string ='';
  color:string = '';
  items=[];
  constructor() { }

  ngOnInit(): void {
  }

  ajouterItem(){
    this.items.push({text: this.text, color: this.color})
  }

}
