import {AfterContentInit, Component, ContentChild, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {FilsComponent} from "../fils/fils.component";
import {TitreComponent} from "../titre/titre.component";

@Component({
  selector: 'app-pere',
  templateUrl: './pere.component.html',
  styleUrls: ['./pere.component.css']
})
export class PereComponent implements OnInit, AfterContentInit {

  @ContentChild(TitreComponent, { static: false }) titre: TitreComponent;

  @ViewChildren(FilsComponent) fils: QueryList<FilsComponent>;
  tab: Array<string> = ['premier', 'deuxieme', 'troisieme'];
  nord = 'Lille';
  sud = 'Marseille';
  capitale = 'Paris';
  constructor() { }

  ngOnInit(): void {

  }

  displayAll(){
    this.fils.forEach(elt => console.log(elt));
  }


  ngAfterContentInit(): void {
    console.log(this);
  }

}
