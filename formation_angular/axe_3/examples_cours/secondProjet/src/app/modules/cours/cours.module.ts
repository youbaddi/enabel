import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoursRoutingModule } from './cours-routing.module';
import {ObservableComponent} from "./observable/observable.component";
import {AdresseComponent} from "./adresse/adresse.component";
import {StagiaireComponent} from "./stagiaire/stagiaire.component";


@NgModule({
  declarations: [
    ObservableComponent,
    AdresseComponent,
    StagiaireComponent,
  ],
  imports: [
    CommonModule,
    CoursRoutingModule
  ]
})
export class CoursModule { }
