import { Component, OnInit } from '@angular/core';
import {interval, Observable, Observer, of} from "rxjs";
import {chainedInstruction} from "@angular/compiler/src/render3/view/util";
import {componentFactoryName} from "@angular/compiler";

@Component({
  selector: 'app-observable',
  templateUrl: './observable.component.html',
  styleUrls: ['./observable.component.css']
})
export class ObservableComponent implements OnInit {

  status = '';
  tab: Array<string> = [];

  constructor() { }

  ngOnInit(): void {
    const observable: Observable<number> = interval(1000);;
    // apartir d'un tableau
    // const tableau = [1, 2, 3];
    // const observable: Observable<number> = from(tableau);
    // example with range
    // const observable: Observable<number> = range(1, 3)
    const observer: Observer<number> = {
      next: (value) => {
        this.tab.push("element : " + value);
      },
      error: (error) => {
        this.status = error;
      },
      complete:   () => {
        this.status = 'fini'
      } };
    observable.subscribe(observer);
  }

}
