import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-adresse',
  templateUrl: './adresse.component.html',
  styleUrls: ['./adresse.component.css']
})
export class AdresseComponent implements OnInit {

  ville: string;
  rue: string;
  codePostal: string;

  nom: string = 'baddi';
  prenom: string = 'youssef';
  constructor(private route: ActivatedRoute,
               private router : Router) { }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(
      res => {
        this.ville = res.get('ville');
        this.rue = res.get('rue');
        this.codePostal = res.get('codepostal');
      } );
  }

  goToStagiaire() {
    console.log("goto");
    this.router.navigateByUrl("/stagiaire/" + this.nom + "/" + this.prenom);
  }

}
