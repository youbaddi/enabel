import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AutomobileRoutingModule } from './automobile-routing.module';
import { VoitureComponent } from './composants/voiture/voiture.component';
import { MotoComponent } from './composants/moto/moto.component';


@NgModule({
  declarations: [VoitureComponent, MotoComponent],
  imports: [
    CommonModule,
    AutomobileRoutingModule
  ]
})
export class AutomobileModule {

  constructor() {
    console.log('automobile-module');
  }
}
