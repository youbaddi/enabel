import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MotoComponent } from './composants/moto/moto.component';
import { VoitureComponent } from './composants/voiture/voiture.component';


const routes: Routes = [
  { path: 'moto', component: MotoComponent },
  { path: 'voiture', component: VoitureComponent },
  { path: '', component: VoitureComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AutomobileRoutingModule { }
