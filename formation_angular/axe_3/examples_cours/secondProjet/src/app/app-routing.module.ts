import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {StagiaireComponent} from "./modules/cours/stagiaire/stagiaire.component";
import {AdresseComponent} from "./modules/cours/adresse/adresse.component";
import {RouterModule, Routes} from "@angular/router";
import {ErrorComponent} from "./composants/error/error.component";
import {MotoComponent} from "./modules/automobile/composants/moto/moto.component";
import {VoitureComponent} from "./modules/automobile/composants/voiture/voiture.component";
import {ReactiveformComponent} from "./composants/reactiveform/reactiveform.component";
import {PersonneComponent} from "./composants/personne/personne.component";
import {PereComponent} from "./composants/pere/pere.component";
import {ListComponent} from "./composants/list/list.component";


const routes: Routes =[
  { path: '', redirectTo: '/stagiaire', pathMatch: 'full' },
  {path: 'cours', loadChildren: () => import('./modules/cours/cours.module').
    then(m => m.CoursModule)},
  { path: 'error', component: ErrorComponent },
  { path: 'reactiveform', component: ReactiveformComponent },

  { path: 'person', component: PersonneComponent },
  { path: 'pere', component: PereComponent },
  { path: 'list', component: ListComponent },



  // { path: 'trainee', redirectTo: '/stagiaire' },
//   { path: 'vehicule', children: [
//       { path: 'moto', component: MotoComponent },
//       { path: 'voiture', component: VoitureComponent },
//       { path: '', component: VoitureComponent }
// ] },
  {path: 'vehicule', loadChildren: () => import('./modules/automobile/automobile.module').
then(m => m.AutomobileModule)},
  { path: '**', redirectTo: 'error' },
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
