import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AdresseComponent } from './modules/cours/adresse/adresse.component';
import { StagiaireComponent } from './modules/cours/stagiaire/stagiaire.component';
import { MenuComponent } from './modules/exercice/menu/menu.component';
import {RouterModule} from "@angular/router";
import { ErrorComponent } from './composants/error/error.component';
import { ObservableComponent } from './modules/cours/observable/observable.component';
import { VoitureModule } from './voiture/voiture.module';
import { VehiculeModule } from "./vehicule/vehicule.module";
import { AutoModule } from './auto/auto.module';
import { AutomobileModule } from './modules/automobile/automobile.module';
import { CoursModule } from './modules/cours/cours.module';
import { ExerciceModule } from './modules/exercice/exercice.module';
import { GetCharPipe } from './pipes/get-char.pipe';


import { TimeAgoExtPipe } from './pipes/time-ago.pipe';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { ReactiveformComponent } from './composants/reactiveform/reactiveform.component';
import { PersonneComponent } from './composants/personne/personne.component';
import { PereComponent } from './composants/pere/pere.component';
import { FilsComponent } from './composants/fils/fils.component';
import { ListComponent } from './composants/list/list.component';
import { ItemComponent } from './composants/item/item.component';
import { TitreComponent } from './composants/titre/titre.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ErrorComponent,
    GetCharPipe,
    TimeAgoExtPipe,
    ReactiveformComponent,
    PersonneComponent,
    PereComponent,
    FilsComponent,
    ListComponent,
    ItemComponent,
    TitreComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    VoitureModule,
    VehiculeModule,
    AutoModule,
    CoursModule,
    ExerciceModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    console.log('app-module');
  }

}
