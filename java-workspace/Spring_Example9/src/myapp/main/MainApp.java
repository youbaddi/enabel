package myapp.main;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import myapp.imp.SimpleCalculator;
import myapp.services.ICalculator;

public class MainApp {
	
	public static void main(String[] args) {

	System.err.println("+++ CalculatorWithLogger");
	String conf = "config.xml";
	// create a context and find beans
	try( ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(conf);)
	{
		SimpleCalculator calc = (SimpleCalculator)ctx.getBean("calculator", ICalculator.class);
		calc.start();
		calc.add(10, 5);
		calc.stop();
	}
	}

}
