package com.mkyong.common;

public class TextEditor {
	
	private SpellChecker spellChecker1;
	private SpellChecker spellChecker2;
	private String name;

	    
	   
	   
	   
	   
	   
	   public String getName() {
		return name;
	}






	public void setName(String name) {
		this.name = name;
	}






	public SpellChecker getSpellChecker1() {
		return spellChecker1;
	}






	public void setSpellChecker1(SpellChecker spellChecker1) {
		System.out.println("set SpellChecker1 prop");
		this.spellChecker1 = spellChecker1;
	}






	public SpellChecker getSpellChecker2() {
		return spellChecker2;
	}






	public void setSpellChecker2(SpellChecker spellChecker2) {
		System.out.println("set SpellChecker2 prop");
		this.spellChecker2 = spellChecker2;
	}






	public void spellCheck() {
	      spellChecker1.checkSpelling();
	      spellChecker2.checkSpelling();
	   }

}
