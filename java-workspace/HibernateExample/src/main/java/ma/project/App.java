package ma.project;

import org.hibernate.Session;

import ma.project.entity.Stock;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        
        // start transaction
        session.beginTransaction();
        
        
        Stock stock = new Stock();
        stock.setStockCode("code1");
        stock.setStockName("stock 1");
        
        session.save(stock);
        // end transaction
        session.getTransaction().commit();
        
        
    }
}
