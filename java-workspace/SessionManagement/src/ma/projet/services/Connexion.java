package ma.projet.services;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class Connexion {

	private static Connection connection;

	static {
		try {
			FileInputStream f = new FileInputStream("/Volumes/Data/Workspace/formations/Csharp/formation_benjeloun/enabel/java-workspace/etudiantManagment/ressources/base.properties");
			Properties p = new Properties();
			p.load(f);
			String url = p.getProperty("jdbc.url");
			String login = p.getProperty("jdbc.username");
			String password = p.getProperty("jdbc.password");
			String driver = p.getProperty("jdbc.driver");
			System.out.println(url);
			System.out.println(login);
			System.out.println(password);
			System.out.println(driver);
			Class.forName(driver);
			connection = DriverManager.getConnection(url, login, password);
			System.out.println(connection);
		} catch (Exception ex) {
			System.out.println("Execption ba " + ex.getMessage());
		}
	}

	public static Connection getConnection() {
		return connection;
	}
}
