package com.mkyong.common;

public class HelloWorld {

	private String name;
	private int age;

	public void setName(String name) {
		this.name = name;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void initialisation() {
		System.out.println("Bean is going through init.");
	}

	public void destroy() {
		System.out.println("Bean will destroy now.");
	}

	public void printHello() {
		System.out.println("Hello ! " + name + " son Age est " + age);
	}

}

