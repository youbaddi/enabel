package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Exercice18Application {

	public static void main(String[] args) {
		SpringApplication.run(Exercice18Application.class, args);
	}

}
