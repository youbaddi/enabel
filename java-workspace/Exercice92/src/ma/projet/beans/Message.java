package ma.projet.beans;

import java.util.Date;

public class Message {
	private int id;
	private String object;
	private String body;
	private Date date;
	private Employe empEmetteur;
	private Employe empRecepteur;
}
