package com.spring.presentation;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Method;
import java.util.Properties;
import java.util.Scanner;

import com.spring.data.Data;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		// get the calss name from prop file
        
		try {
			FileInputStream f = new FileInputStream("/Volumes/Data/Workspace/formations/formation_benjeloun/enabel/java-workspace/spring_example4/src/config.proprities");
		
		Properties p = new Properties();
		p.load(f);
		String classMetier = p.getProperty("metier");
        System.out.println(classMetier);
        String classData = p.getProperty("data");
        System.out.println(classData);
        
        //
        
           // get class metier   injection d'une depandance
			Class clMetier = Class.forName(classMetier);
			Object o1 = clMetier.newInstance();
			
			
			
			 // get class data  injection d'une depandance
			Class clData = Class.forName(classData);
			Object o2 = clData.newInstance();
			
			
			
			
			Method md1 = clMetier.getMethod("setData", Data.class);
			md1.invoke(o1, o2);
			
			Method md2 = clMetier.getMethod("calculePrice", null);
			System.out.println(md2.invoke(o1, null));
			
			
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
