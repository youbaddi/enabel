package com.spring.interfaces;

public interface IData {
   public double computePrice();
}
