package com.spring.data;

public abstract class Data {

	public abstract double computePrice();
}
