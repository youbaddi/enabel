package com.spring.metier;

import com.spring.data.Data;
import com.spring.interfaces.IData;
import com.spring.interfaces.IMetier;

public class MetierB implements IMetier{

	
private Data data;
	
	public Data getData() {
		// TODO Auto-generated method stub
		return data;
	}

	
	public void setData(Data newdata) {
		// TODO Auto-generated method stub
		data = newdata;
	}
	
	@Override
	public double calculePrice() {
		// TODO Auto-generated method stub
		System.out.println("calculePrice MetierB");
		double res = this.getData().computePrice() + (this.getData().computePrice() * 5/100);
		return res;
	}

}