package com.mkyong.common;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");

		HelloWorld obj = (HelloWorld) context.getBean("helloBean");
		obj.printHello();
		
		
		HelloWorld obj1 = (HelloWorld) context.getBean("helloBean1");
		obj1.printHello();
		
		
		HelloWorld obj2 = (HelloWorld) context.getBean("helloBean1");
		obj2.printHello();

    }
}
