package ma.projet.connexion;

import java.sql.DriverManager;
import java.sql.SQLException;

import java.sql.Connection;



public class Connexion {
	private static String driverName = "com.mysql.jdbc.Driver";
	private static String url = "jdbc:mysql://192.168.57.103/enabel";
	private static String login ="root";
	private static String password = "baddiroot";
	private static Connection cn;
	
	static {
		try {
		Class.forName(driverName);
		
		cn = DriverManager.getConnection(url,login,password);
		}catch (ClassNotFoundException e) {
			// TODO: handle exception
			System.out.println("Impossible de charger le driver");
		}catch(SQLException ex) {
			System.out.println("Error de connexion");
		}
	}
	
	public static  Connection  getCn(){
		return cn;
	}
}
