package ma.projet.service;

import java.sql.ResultSet;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.ArrayList;
import ma.projet.beans.Client;
import ma.projet.connexion.Connexion;
import ma.projet.dao.IDao;

public class ClientService implements IDao<Client>{

	@Override
	public boolean create(Client o) {
		// TODO Auto-generated method stub
		try {
		String query = "INSERT INTO client (nom,prenom) "
				+ "values ('"+o.getNom()+"','"+o.getPrenom()+"');";
		System.out.println(query);
		Statement st = Connexion.getCn().createStatement();
		if(st.executeUpdate(query) ==1)
			return true;
		}catch (SQLException e) {
			// TODO: handle exception
			System.out.println("Erreur SQL");
		}
		return false;
	}

	@Override
	public boolean delete(Client o) {
		// TODO Auto-generated method stub
		try {
			String query = "DELETE FROM client where id="+o.getId()+";";
			System.out.println(query);
			Statement st = Connexion.getCn().createStatement();
			if(st.executeUpdate(query) ==1)
				return true;
			}catch (SQLException e) {
				// TODO: handle exception
				System.out.println("Erreur SQL");
			}
			return false;
	}

	@Override
	public boolean update(Client o) {
		// TODO Auto-generated method stub
		try {
			String query = "UPDATE client set nom='"+o.getNom()+
					"', prenom='"+o.getPrenom()+
					"' where id="+o.getId()+";";
			System.out.println(query);
			Statement st = Connexion.getCn().createStatement();
			if(st.executeUpdate(query) ==1)
				return true;
			}catch (SQLException e) {
				// TODO: handle exception
				System.out.println("Erreur SQL");
			}
			return false;
	}

	@Override
	public Client findById(int id) {
		// TODO Auto-generated method stub
		Client result=null;
		try {
			String query = "SELECT * From client where "
					+ "id="+id+";";
			System.out.println(query);
			Statement st = Connexion.getCn().createStatement();
			ResultSet rs =  st.executeQuery(query);
			if(rs.next())
				result = new Client(rs.getInt("id"),
						rs.getString("nom"),
						rs.getString("prenom"));
				return result;
			}catch (SQLException e) {
				// TODO: handle exception
				System.out.println("Erreur SQL");
			}
			return result;
	}

	@Override
	public List<Client> findAll() {
		// TODO Auto-generated method stub
		List<Client> result= new ArrayList<>();
		try {
			String query = "SELECT * From client;";
			System.out.println(query);
			Statement st = Connexion.getCn().createStatement();
			ResultSet rs =  st.executeQuery(query);
			while(rs.next()) {
				result.add(new Client(rs.getInt("id"),
						rs.getString("nom"),
						rs.getString("prenom")));
			}
			}catch (SQLException e) {
				// TODO: handle exception
				System.out.println("Erreur SQL");
			}
			return result;
	}

}
