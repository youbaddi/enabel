package ma.project.test;

import java.util.List;

import ma.projet.beans.Client;
import ma.projet.service.ClientService;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ClientService sc = new ClientService();
		Client c1 = new Client("Ahmed", "ferkous");
		Client c2 = new Client("yasmine", "Lahdar");
		Client c3 = new Client("Sara", "Kiass");
		Client c4 = new Client("Younes", "Mesaaf");
		Client c5 = new Client("Hamza", "barouni");
		System.out.println(c1);
		System.out.println(c2);
		System.out.println(c3);
		System.out.println(c4);
		System.out.println(c5);
		
		sc.create(c1);
		sc.create(c2);
		sc.create(c3);
		sc.create(c4);
		sc.create(c5);
		Client c6 = sc.findById(22);
		System.out.println(c6);
		if(c6!=null)sc.delete(c6);
		
		
		
		List<Client> clients = sc.findAll();
		for (Client client : clients) {
			System.out.println(client);
		}
		
		
		Client c7 = sc.findById(28);
		
		if(c7!=null) {
			System.out.println(c7);
			c7.setNom("SaraEdited");
			c7.setPrenom("KiassEdited");
			sc.update(c7);
		}
		
		
		
	}

}
