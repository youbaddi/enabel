package ch4.ex57;

public class PointNom extends Point {

	private char nom;
	
	public PointNom(int x, int y, char nom) {
		// TODO Auto-generated constructor stub
		// method1
		super(x,y);
		
		this.nom=nom;
	}
	
	public void affCoordNom(){
		System.out.println("Point de nom " + nom);
		affCoord();
	}
	
	public void setPointNom(int x, int y, char nom){
		//this.x = x;
		//this.y = y;
		setPoint(x,y);
		this.nom = nom;
	}
	
	

	public void setNom(char nom){
		this.nom = nom;
	}
	
	public static void main(String[] args) {
		PointNom pn = new PointNom(3, 5, 'A');
		pn.affCoordNom();
		pn.affCoord();
		
		Point p = new Point(3,2);
		p.affCoord();
		p.setPoint(6, 7);
		p.affCoord();
	}
	
	
}
