package ex28;

public class Exercice28 {
	
	public static boolean isBessixtille(int year) {
		
		if(((year%4==0 && year%100!=0) || year%400==0) == true)
			return true;
		else return false;
//		return ((year%4==0 && year%100!=0) || year%400==0);
		
	}
	
	public static void main(String[] args) {
		/* if the function is static we use directly the class to call the function 
		 * */
//		System.out.println(Exercice28.isBessixtille(2020));
		
		/* if the function is not static */
//		Exercice28 ex = new Exercice28();
//		System.out.println(ex.isBessixtille(2020));
		
		
		System.out.println(isBessixtille(2020));
		
	}

}
