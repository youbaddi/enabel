package ch5.ex65;

public abstract class Figure {
	
	public abstract void affiche();
	public abstract void homothetie (double coeff);
	public abstract void rotation (double angle);

}
