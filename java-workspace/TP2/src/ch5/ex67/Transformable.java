package ch5.ex67;

public interface Transformable {
	public void homothetie(double coef);
	public void rotation (double angle);
}
