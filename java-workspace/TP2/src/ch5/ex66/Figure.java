package ch5.ex66;

public abstract class Figure {

	public abstract void affiche();

	public abstract void homothetie(double coeff);

	public abstract void rotation(double angle);

	public void homoRot(double coeff, double angle) {
		homothetie(coeff);
		rotation(angle);
	}

	static void afficheFigures(Figure[] f) {
		for (int i = 0; i < f.length; i++)
			f[i].affiche();
	}

	static void homothetieFigures(Figure[] f, double coeff) {
		for (int i = 0; i < f.length; i++)
			f[i].homothetie(coeff);
	}

	static void rotationFigures(Figure[] f, double angle) {
		for (int i = 0; i < f.length; i++)
			f[i].rotation(angle);
	}

}
