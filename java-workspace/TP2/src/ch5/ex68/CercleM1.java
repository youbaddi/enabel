package ch5.ex68;

public class CercleM1 extends Point{
	public  double r;

	public CercleM1(double x, double y, double r) {
		// TODO Auto-generated constructor stub
		super(x, y);
		this.r = r;
	}
	
	public void deplaceCentre(double dx, double dy){
		super.deplace(dx, dy);
	}

	public void changeRayon(double r) {
		this.r = r;
	}
	
	
	public Point getCentre(){
		Point p = new Point(getX(),getY());
		return p;
	}
	
	
	public void affiche(){
		System.out.println("cercle de coordonne  "+ super.getX()+ " "+ getY() + " de Rayon " + r);
	}
	
	
	public static void main(String[] args) {
		CercleM1 c = new CercleM1(4, 5, 6);
		c.affiche();
		c.deplaceCentre(3, 2);
		c.changeRayon(7);
		c.affiche();
		Point a = c.getCentre()	;
//		c.getCentre().affiche();
				a.affiche();
		}
	
	
}
