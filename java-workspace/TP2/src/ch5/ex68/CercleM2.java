package ch5.ex68;

public class CercleM2 {
	public Point centre;
	public  double r;

	public CercleM2(double x, double y, double r) {
		// TODO Auto-generated constructor stub
		this.centre = new Point(x, y);
		this.r = r;
	}
	
	public void deplaceCentre(double dx, double dy){
		centre.deplace(dx, dy);
	}

	public void changeRayon(double r) {
		this.r = r;
	}
	
	
	public Point getCentre(){
		
		return centre;
	}
	
	
	public void affiche(){
		System.out.println("cercle de coordonne  "+ centre.getX()+ " "+ centre.getY() + " de Rayon " + r);
	}
	
	
	public static void main(String[] args) {
		CercleM2 c = new CercleM2(4, 5, 6);
		c.affiche();
		c.deplaceCentre(3, 2);
		c.changeRayon(7);
		c.affiche();
		Point a = c.getCentre()	;
				a.affiche();
		}
	
	
}
