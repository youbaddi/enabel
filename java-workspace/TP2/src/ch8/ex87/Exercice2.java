package ch8.ex87;

import java.util.ArrayList;
import java.util.Iterator;

public class Exercice2 {

	
	public static void main(String[] args) {
		ArrayList<String> colors = new ArrayList<String>();
		colors.add("Red");
		colors.add("Blue");
		colors.add("Green");
		colors.add("Orange");
		colors.add("Black");
		
		Iterator<String> it = colors.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		
		
		
		for(String s : colors) {
			System.out.println(s);
		}

		
	}
}
