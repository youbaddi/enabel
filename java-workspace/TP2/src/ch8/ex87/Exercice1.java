package ch8.ex87;

import java.util.ArrayList;

public class Exercice1 {

	
	public static void main(String[] args) {
		ArrayList<String> colors = new ArrayList<String>();
		colors.add("Red");
		colors.add("Blue");
		colors.add("Green");
		colors.add("Orange");
		colors.add("Black");
		System.out.println(colors);
	}
}
