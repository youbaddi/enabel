package ch8.ex87;

import java.util.ArrayList;
import java.util.Iterator;

public class Exercice7 {

	
	public static void main(String[] args) {
		ArrayList<String> colors = new ArrayList<String>();
		colors.add("Red");
		colors.add("Blue");
		colors.add("Green");
		colors.add("Orange");
		colors.add("Black");
		
		colors.add(0, "Yelow");
		colors.add(5, "Pink");
		
		Iterator<String> it = colors.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		
		System.out.println("----------");
		String element = colors.get(0);
		System.out.println(element);
		System.out.println("----------");
		element = colors.get(5);
		System.out.println(element);
		
		
		System.out.println("----------");
		colors.set(5, "White");
		Iterator<String> it1 = colors.iterator();
		while(it1.hasNext()) {
			System.out.println(it1.next());
		}
		
		System.out.println("----------");
		colors.remove(2);
		for (String string : colors) {
			System.out.println(string);
		}
		
		System.out.println("----------");
		if(colors.contains("orange")) {
			System.out.println("existe");
		}else {
			System.out.println("doesn't exist");
		}
		
		colors.remove(2);
		for (String string : colors) {
			System.out.println(string);
		}
		
//		
//		for(String s : colors) {
//			System.out.println(s);
//		}

		
	}
}
