package ch7.ex84;


class TabPlein extends Exception{}

public abstract class TabTrie {
	
	Object[] tab;
	static int capacite = 10;
	int pos;
	
	public TabTrie() {
		// TODO Auto-generated constructor stub
		tab = new Object[capacite];
		pos=0;
	}
	
	abstract boolean plusGrand(Object o1, Object o2);
	void ajouter(Object o) throws TabPlein{
		Object temp;
		try {
		tab[pos]=o;
		int i = pos;
		while((i>0) && plusGrand(tab[i-1],tab[i])) {
			temp = tab[i-1];
			tab[i-1] = tab[i];
			tab[i] = temp;
			i--;
		}
		
		pos++;
		}catch (ArrayIndexOutOfBoundsException e) {
			// TODO: handle exception
			throw new TabPlein();
		}
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String res = "[";
		for (int i = 0; i < tab.length; i++) {
			res +=tab[i] + "; ";
		}
		res +="]";
		return res;
	}

}
