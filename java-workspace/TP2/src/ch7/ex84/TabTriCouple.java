package ch7.ex84;


class Couple{
	int x,y;
	
	
	public Couple(int x, int y) {
		// TODO Auto-generated constructor stub
		this.x=x;
		this.y=y;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Couple (x = " + x + ", y = " +y +")";
	}
}

public class TabTriCouple extends TabTrie {

	@Override
	boolean plusGrand(Object o1, Object o2) {
		// TODO Auto-generated method stub
		Couple c1 = (Couple)o1;
		Couple c2 = (Couple)o2;
		return (c1.x > c2.x) || ((c1.x == c2.x)&& (c1.y > c2.y));
	}
	
	public static void main(String[] args) throws TabPlein {
		TabTriCouple t = new TabTriCouple();
		for(int i =0; i< capacite;i++) {
			t.ajouter(new Couple((int)(Math.random()*1000), 
					(int)(Math.random()*1000)));
		}
		System.out.println(t.toString());
	}
	

}
