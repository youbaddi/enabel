package ch7.ex82;

public class Entreprises {

		public static void allEntreprise(Entreprise[] entreprises){
			for (int i =0; i< entreprises.length; i++) {
				try {
					System.out.println(entreprises[i].mission());
				} catch (SecretMissionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				try {
					System.out.println(entreprises[i].capital());
				} catch (NoProfitException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		public static void main(String[] args) {
			Entreprise[] entreprises = new Entreprise[4];
			Entreprise ford = new Entreprise("Ford", "mission1", 1000, 1200);
			Entreprise apple = new Entreprise("Apple", "mission2", 1000, 1200);
			Entreprise shell = new Entreprise("Shell", "mission3", 1000, 1200);
			Entreprise audi = new Entreprise("Audi", "mission4", 1000, 1200);
			
			entreprises[0] = ford;
			entreprises[1] = apple;
			entreprises[2] = shell;
			entreprises[3] = audi;
			allEntreprise(entreprises);
			
		}
}
