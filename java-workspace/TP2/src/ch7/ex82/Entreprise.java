package ch7.ex82;


class SecretMissionException extends Exception{}
class NoProfitException extends Exception{}

public class Entreprise {
	private String nom, mission;
	private int nmbEmployes, capital;
	public Entreprise(String nom, String mission, int nmbEmployes, int capital) {
		this.nom = nom;
		this.mission = mission;
		this.nmbEmployes = nmbEmployes;
		this.capital = capital;
	}
	
	
	public String mission() throws SecretMissionException{
		return mission;
	}
	
	public int capital() throws NoProfitException{
		return capital;
	}
	
	
}


 class EntrepriseSecrete extends Entreprise{
	 
	 public EntrepriseSecrete(String nom, String mission, int nmbEmployes, int capital) {
		super(nom, mission, nmbEmployes, capital);
		// TODO Auto-generated constructor stub
	}

	public String mission() throws SecretMissionException{
		 throw new SecretMissionException();
	 }
 }
 
 
 class EntrepriseSansProfit extends Entreprise{
	 
	 public EntrepriseSansProfit(String nom, String mission, int nmbEmployes, int capital) {
		super(nom, mission, nmbEmployes, capital);
		// TODO Auto-generated constructor stub
	}

	public int capital() throws NoProfitException{
		 throw new NoProfitException();
	 }
 }
