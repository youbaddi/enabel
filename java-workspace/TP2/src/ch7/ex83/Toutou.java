package ch7.ex83;

public class Toutou {
	private String nom;
	private int nombrePuces;
	public Toutou(String nom, int nombrePuces) throws IllegalArgumentException{
		if(nom == null)
			throw new IllegalArgumentException("pas de nom!");
		this.nom = nom;
		if(nombrePuces < 0)
				throw new IllegalArgumentException("nomber de puce est negatif");	
		this.nombrePuces = nombrePuces;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Toutou de nom " + nom +" ayant " + nombrePuces + " puce(s)";
	}
	
	
	public static void main(String[] args) {
		try {
		Toutou t1 = new Toutou("name1", 10);
		
		System.out.println(t1);
		
		Toutou t2 = new Toutou("name2", -10);
		System.out.println(t2);
		}catch (IllegalArgumentException e) {
			// TODO: handle exception
			System.out.println("IllegalArgumentException !!");
		}
	}
	
	
}
