package ch7.ex73;

public class EntNat {
	private int n;
	
	public EntNat(int n) throws ErrConst{
		// TODO Auto-generated constructor stub
		if (n<0) throw new ErrConst(n);
		this.n = n;
	}

	public int getN() {
		return n;
	}

}

class ErrConst extends Exception{
	private int n;
	
	public ErrConst(int n) {
		// TODO Auto-generated constructor stub
		this.n = n;
	}
	
	public int getN() {
		return n;
	}
	
}
