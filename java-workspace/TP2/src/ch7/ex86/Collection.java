package ch7.ex86;

class Collection {
	 
    public static void main(String[] args) {
        Ustensile[] us = new Ustensile[5];
        us[0] = new AssietteRonde(1926, 8.4);
        us[1] = new Cuillere(1881, 7.3);
        us[2] = new AssietteCarree(1935, 5.6);
        us[3] = new Cuillere(1917, 8.8);
        us[4] = new AssietteRonde(1837, 5.4);
 
        afficherCuilleres(us);
        afficherSurfaceAssiettes(us);
        afficherValeurTotale(us);
    }
 
    static void afficherCuilleres(Ustensile[] us) {
        int nbCuilleres = 0;
        for (int i = 0; i < us.length; i++) {
            // A compléter
        	if(us[i] instanceof Cuillere) nbCuilleres++;
        }
        System.out.println("Il y a " + nbCuilleres + " cuillères.");
    }
 
    static void afficherSurfaceAssiettes(Ustensile[] us) {
        double somme = 0;
        for (int i = 0; i < us.length; i++) {
            // A compléter
        	if(us[i] instanceof Assiette) somme += ((Assiette)us[i]).calculeSurface();
        }
        System.out.println("Surface totale des assiettes : " + somme);
    }
 
    static void afficherValeurTotale(Ustensile[] us) {
        double somme = 0;
        for (int i = 0; i < us.length; i++) {
            // A compléter
        	somme += us[i].calculeValeur(2020);
        }
        System.out.println("Valeur totale de la collection : " + somme);
    }
}


abstract class Ustensile{
	private int annee;

	public Ustensile(int annee) {
		this.annee = annee;
	}
	
	public double calculeValeur(int date) {
		int age = date - annee;
		double valeur=0;
		if(age > 50)
			valeur =  age - 50;
		return valeur;
	}
}


abstract class Assiette extends Ustensile{
	
	public Assiette(int annee) {
		// TODO Auto-generated constructor stub
		super(annee);
	}
	
	public abstract double calculeSurface();
}


 class AssietteRonde extends Assiette{
	private double rayon;

	public AssietteRonde(int annee, double rayon) {
		super(annee);
		this.rayon = rayon;
	}
	
	@Override
	public double calculeSurface() {
		// TODO Auto-generated method stub
		return (3.14 * rayon * rayon);
	}
	
}


 class AssietteCarree extends Assiette{
	private double cote;

	public AssietteCarree(int annee, double cote) {
		super(annee);
		this.cote = cote;
	}
	
	@Override
	public double calculeSurface() {
		// TODO Auto-generated method stub
		return cote * cote;
	}
	
	public double calculeValeur(int date) {
		double valeur=5 * super.calculeValeur(date);
		return valeur;
	}
	
}

 class Cuillere extends Ustensile{
	private double longeur;

	public Cuillere(int annee, double longeur) {
		super(annee);
		this.longeur = longeur;
	}
	
	
	
}