package ex18;

import java.util.Scanner;

public class Clavier {
	
	public static String lireText(){
		String resultat;
		Scanner sc = new Scanner(System.in);
		resultat = sc.next();
		return resultat;
	}

	public static int lireInt(){
		int resultat;
		Scanner sc = new Scanner(System.in);
		resultat = sc.nextInt();
		return resultat;
	}

	public static double lireDouble() {
		double resultat;
		Scanner sc = new Scanner(System.in);
		resultat = sc.nextDouble();
		return resultat;
	}
}
