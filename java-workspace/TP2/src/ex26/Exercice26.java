package ex26;

import ex18.Clavier;

public class Exercice26 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double x;
		do {
			System.out.println("donnez un nombre positif: ");
			x= Clavier.lireDouble();
			if(x<0) {System.out.println("svp positif");}
			if(x<=0) {continue;}
			System.out.println("sa racine carre est "+ Math.sqrt(x));
		}while(x!=0);

	}

}
