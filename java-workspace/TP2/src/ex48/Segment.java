package ex48;

public class Segment {

	private Point or, ext;
	
	

	public Segment(Point origine, Point extremite) {
		this.or = origine;
		this.ext = extremite;
	}

	public Segment(double xOr, double yOr, double xExt, double yExt) {
//		this.or=new Point(xOr,yOr);
//		this.ext = new Point(xExt,yExt);
		this(new Point(xOr, yOr), new Point(xExt, yExt));
	}

	double longueur() {
		double xExt = this.ext.getX();
		double yExt = this.ext.getY();
		double xOr = this.or.getX();
		double yOr = this.or.getY();
		return Math.sqrt(Math.pow((xExt - xOr), 2) + Math.pow((yExt - yExt), 2));
	}

	void deplaceOrigine(double dx, double dy) {
         or.setX(or.getX() + dx);
         or.setY(or.getY() + dy);
	}

	void deplaceExtremite(double dx, double dy) {
		ext.setX(ext.getX() + dx);
		ext.setY(ext.getY() + dy);
	}

	void affiche() {
		System.out.println("Origine - ") ; or.affiche();
		System.out.println("Exterm - ") ; ext.affiche();
	}

	public static void main(String[] args) {
         Segment s = new Segment(3, 4, 6, 10);
         s.affiche();
         System.out.println(s.longueur());
         s.deplaceOrigine(3, 5);
         s.affiche();
         System.out.println(s.longueur());
         s.deplaceExtremite(6, 4);
         s.affiche();
         System.out.println(s.longueur());
	}

}




