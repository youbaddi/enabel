package ex30;

public class Exercice30 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int nbf;
		int c2;
		int c5;
		int c10;

		nbf = 0;
		c10 = 0;
		c5 = 0;
		c2 = 0;
		// for (c10 = 0; c10 <= 10; c10++) {
		while (c10 <= 10) {
			// for (c5 = 0; c5 <= 20; c5++) {
			c5 = 0;
			c2 = 0;
			while (c5 <= 20) {
				c2 = 0;
				while (c2 <= 50) {
					// for (c2 = 0; c2 <= 50; c2++) {
					if (2 * c2 + 5 * c5 + 10 * c10 == 100) {
						nbf++;
						System.out.print("1 F = ");
						if (c2 != 0)
							System.out.printf("%d x 2c", c2);
						if (c5 != 0) {
							if (c2 != 0)
								System.out.print(" + ");
							System.out.printf("%d x 5c", c5);
						}

						if (c10 != 0) {
							if (c2 != 0 || c5 != 0)
								System.out.print(" + ");
							System.out.printf("%d x 10c", c10);
						}
						System.out.println();
					}
					c2++;
				}
				c5++;
			}
			c10++;
		}

		System.out.println("En tout, il y a " + nbf + " facons de faire 1F");

	}

}
