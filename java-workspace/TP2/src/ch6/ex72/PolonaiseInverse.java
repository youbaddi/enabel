package ch6.ex72;

import java.util.Scanner;

public class PolonaiseInverse {
	
	
	public static int eval(String s) {
		Pile p = new PileParTableau(100);
		
		for(int i =0; i<s.length(); i++) {
			char c = s.charAt(i);
			if((c>= '0') && (c<='9')) {
				p.empiler((int) (c - '0') );
			}
			else if((c== '+') || (c=='-') || (c== '*') || (c=='/')) {
				int x=0;
				int y=0;
				if(!p.isPileVide()) {
				y=p.sommet();						
				p.depiler();
				}
				
				if(!p.isPileVide()) {
				x=p.sommet();						
				p.depiler();
				}else {
					System.out.println("expression incorrect ");
					return 0;
				}
				int res=0;
				switch (c) {
				case '+':
					res  = x + y;
					break;

				case '-':
					res  = x - y;
					break;
					
				case '*':
					res  = x * y;
					break;
					
				case '/':
					res  = x / y;
					break;
				}
				p.empiler(res);
			}
		}
		
		return p.sommet();
		
	}
	
	public static void main(String[] args) {
		System.out.println(((int)'a'));
//		String s;
//		Scanner sc = new Scanner(System.in);
//		do {
//		System.out.println("Entrez une expression polonaise a evaluer: ");
//		s = sc.nextLine();
//		if(s.length()!=0) System.out.println("-> resultat : " + eval(s));
//		}while(s.length()!=0);
	}

}
