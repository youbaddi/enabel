package ch6.ex72;

public class PileParChaine implements Pile {
	
	private Noeud n = new Noeud(0, null);
	
	private Noeud sommet = n;

	@Override
	public void effacer() {
		// TODO Auto-generated method stub
		sommet = n;
	}

	@Override
	public void empiler(int element) {
		// TODO Auto-generated method stub
		Noeud newNoeud = new Noeud(element, sommet);
        sommet = newNoeud;
	}

	@Override
	public void depiler() {
		// TODO Auto-generated method stub
		if(sommet == n) {
			System.out.println("Impossible de depiler: la pile est vide");
			System.exit(-1);
		}else 
			sommet = sommet.getPrevious();

	}

	@Override
	public boolean isPileVide() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int sommet() {
		// TODO Auto-generated method stub
		return 0;
	}

}


class Noeud {
	private int value;
	private Noeud previous;
	
	
	public Noeud(int value, Noeud previous) {
		super();
		this.value = value;
		this.previous = previous;
	}


	public int getValue() {
		return value;
	}


	public Noeud getPrevious() {
		return previous;
	}
	
	
	
	
}









