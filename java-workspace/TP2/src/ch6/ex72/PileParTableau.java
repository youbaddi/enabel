package ch6.ex72;

public class PileParTableau implements Pile {
	private int tailleMAx = 1000;
	private int sommet = -1;
	private int[] tableau;
	
	
	
	

	public PileParTableau(int tailleMAx) {
		this.tailleMAx = tailleMAx;
		tableau = new int[tailleMAx];
	}

	@Override
	public void effacer() {
		// TODO Auto-generated method stub
		sommet = -1;

	}

	@Override
	public void empiler(int element) {
		// TODO Auto-generated method stub
		if(sommet == tailleMAx -1) {
			System.out.println("Impossible d'empiler: la pile est pleine");
			System.exit(-1);
		}
		
		sommet ++;
		tableau[sommet] = element;

	}

	@Override
	public void depiler() {
		// TODO Auto-generated method stub
		if(isPileVide()) {
			System.out.println("Impossible de depiler: la pile est vide");
			System.exit(-1);
		}
		
		sommet --;

	}

	@Override
	public boolean isPileVide() {
		// TODO Auto-generated method stub
		return sommet == -1;
	}

	@Override
	public int sommet() {
		// TODO Auto-generated method stub
		if(isPileVide()) {
			System.out.println("Impossible de depiler: la pile est vide");
			System.exit(-1);
		}
		return tableau[sommet];
	}

}
