package ch6.ex72;

public interface Pile {
	public void effacer(); // //vide la pile
	public void empiler(int element); // //empile element dans la pile.
	public void depiler(); // dépile l'élément au sommet de la pile.

	public boolean isPileVide(); // testant si la pile est vide
	public int sommet(); // retournant la valeur du sommet de la pile
}
