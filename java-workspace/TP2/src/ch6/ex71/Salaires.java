package ch6.ex71;

import javax.swing.text.html.HTMLDocument.HTMLReader.IsindexAction;

abstract class Employe {
	private String nom;
	private String prenom;
	private int age;
	private String date;

	public Employe(String nom, String prenom, int age, String date) {
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
		this.date = date;
	}

	public abstract double calculerSalaire();
 public abstract String getJob();
	public String getNom() {
		return getJob() + nom + " " + prenom;
	}

}

abstract class Commercial extends Employe {
	private double chiffreAffaire;

	public Commercial(String nom, String prenom, int age, String date, double chiffreAffaire) {
		super(nom, prenom, age, date);
		this.chiffreAffaire = chiffreAffaire;
	}

	public double getChiffreAffaire() {
		return chiffreAffaire;
	}

	

}

class Vendeur extends Commercial {
	private final static double POURCENTAGE_VENDEUR= 0.2;
	private final static int PRIME_VENDEUR= 400;

	public Vendeur(String nom, String prenom, int age, String date, double chiffreAffaire) {
		super(nom, prenom, age, date, chiffreAffaire);
		// TODO Auto-generated constructor stub
	}
	
	
	@Override
	public double calculerSalaire() {
		// TODO Auto-generated method stub
		return (POURCENTAGE_VENDEUR * getChiffreAffaire()) + PRIME_VENDEUR;
	}


	@Override
	public String getJob() {
		// TODO Auto-generated method stub
		return "Vendeur " ;
	}
}

class Representant extends Commercial {

	private final static double POURCENTAGE_REPRESENTANT= 0.2;
	private final static int PRIME_REPRESENTANT= 800;
	
	
	public Representant(String nom, String prenom, int age, String date, double chiffreAffaire) {
		super(nom, prenom, age, date, chiffreAffaire);
		// TODO Auto-generated constructor stub
	}

	
	@Override
	public double calculerSalaire() {
		// TODO Auto-generated method stub
		return (POURCENTAGE_REPRESENTANT * getChiffreAffaire()) + PRIME_REPRESENTANT;
	}
	
	@Override
	public String getJob() {
		// TODO Auto-generated method stub
		return "Representant " ;
	}
}

class Technicien extends Employe {
	
	private final static double PRIME_UNITE= 5.0;
	private int unite;
	
	public Technicien(String nom, String prenom, 
			int age, String date, int unite) {
		super(nom, prenom, age, date);
		// TODO Auto-generated constructor stub
		this.unite = unite;
	}

	@Override
	public double calculerSalaire() {
		// TODO Auto-generated method stub
		return PRIME_UNITE * unite;
	};
	
	@Override
	public String getJob() {
		// TODO Auto-generated method stub
		return "Technicien " ;
	}
}

class Manutentionnaire extends Employe{

	private final static double PRIME_HEUR= 65.0;
	private int heur;
	
	public Manutentionnaire(String nom, String prenom, 
			int age, String date, int heur) {
		super(nom, prenom, age, date);
		// TODO Auto-generated constructor stub
		this.heur = heur;
	}

	@Override
	public double calculerSalaire() {
		// TODO Auto-generated method stub
		return PRIME_HEUR * heur;
	};
	
	@Override
	public String getJob() {
		// TODO Auto-generated method stub
		return "Manutentionnaire " ;
	}

}


interface ARisque{
	int PRIME = 200;
}

class TechnARisque extends Technicien implements ARisque{

	public TechnARisque(String nom, String prenom, int age, String date, int unite) {
		super(nom, prenom, age, date, unite);
		// TODO Auto-generated constructor stub
	}

	
	@Override
	public double calculerSalaire() {
		// TODO Auto-generated method stub
		return super.calculerSalaire() + PRIME;
	}
	
	@Override
	public String getJob() {
		// TODO Auto-generated method stub
		return "TechnARisque " ;
	}
}

class ManutARisque extends Manutentionnaire implements ARisque{

	public ManutARisque(String nom, String prenom, int age, String date, int heur) {
		super(nom, prenom, age, date, heur);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public double calculerSalaire() {
		// TODO Auto-generated method stub
		return super.calculerSalaire() + PRIME;
	}
	
	@Override
	public String getJob() {
		// TODO Auto-generated method stub
		return "ManutARisque " ;
	}

}

class Personnel {
	
	private Employe[] personnels;
	private int nbEmplye;
	private final static int MAXEMPLOYER = 200;
	
	public Personnel() {
		// TODO Auto-generated constructor stub
		personnels = new Employe[MAXEMPLOYER];
		nbEmplye = 0;
	}
	

	public void ajouterEmploye(Employe e) {
		 ++nbEmplye;  // nbEmplye++;
		if(nbEmplye <= MAXEMPLOYER) {
			personnels[nbEmplye-1] = e;
			
		}else {
			System.out.println("Pas plus de " + MAXEMPLOYER + " employes");
		}

	}

	public void afficherSalaires() {
		for(int i = 0; i< nbEmplye; i++) {
			System.out.println(personnels[i].getNom() + " gagne " + 
			personnels[i].calculerSalaire() + " MAD");
		}
		
		

	}

	public double salaireMoyen() {
		double resultat=0.0;
		for(int i = 0; i< nbEmplye; i++) {
			resultat += personnels[i].calculerSalaire() ;
		}
		return resultat/nbEmplye;
	}
	
	public double salaireMoyenVendeurs() {
		double resultat=0.0;
		int nbOfVendeur = 0;
		for(int i = 0; i< nbEmplye; i++) {
			if(personnels[i] instanceof Vendeur) {
			   resultat += personnels[i].calculerSalaire() ;
			   nbOfVendeur++;
			}
		}
		return resultat/nbOfVendeur;
	}
	
}

public class Salaires {

	public static void main(String[] args) {
		Personnel p = new Personnel();
		p.ajouterEmploye(new Vendeur("Pierre", "Business", 45, "1995", 30000));
		p.ajouterEmploye(new Representant("Léon", "Vendtout", 25, "2001", 20000));
		p.ajouterEmploye(new Technicien("Yves", "Bosseur", 28, "1998", 1000));
		p.ajouterEmploye(new Manutentionnaire("Jeanne", "Stocketout", 32, "1998", 45));
		p.ajouterEmploye(new TechnARisque("Jean", "Flippe", 28, "2000", 1000));
		p.ajouterEmploye(new ManutARisque("Al", "Abordage", 30, "2001", 45));

		p.afficherSalaires();
		System.out.println("Le salaire moyen dans l'entreprise est de " + p.salaireMoyen() + " MAD.");
	}

}
