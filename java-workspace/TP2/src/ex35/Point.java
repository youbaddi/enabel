package ex35;

class Point
{
	public Point(int abs,int ord) {x=abs; y=ord;}
    public void deplace(int dx,int dy){x+=dx; y+=dy;}
    
    public double getX() {
		return x;
	}
    
    public double getY() {
		return y;
	}
    
   private double x ; // abscisse
   private double y ; // ordonnee
}
