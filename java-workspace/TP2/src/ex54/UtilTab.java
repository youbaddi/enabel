package ex54;

import ex45.Util;

public class UtilTab {
	
	public static int[] genere(int n) {
		int[] res = new int[n];
		for(int i=0, j=1;i<n;i++,j+=2) 
			//res[i] = 2*i+1 ;
			res[i]=j;
		return res;
	}
	
	
	public static int[] somme(int[] t1, int[] t2) {
		if(t1.length != t2.length) {
			System.out.println("erreur taille not egual");
		return null;
		}
		int[] t3 = new int[t1.length];
		for(int i=0;i<t1.length;i++) t3[i] = t2[i] + t1[i] ;
		return t3;
	}
	
	public static void affiche(int[] ta) {
		// TODO Auto-generated method stub
		if(ta!=null) {
		for(int i=0;i<ta.length;i++) System.out.println(ta[i]);
		}
	}
	
	public static void main(String[] args) {
		int[] ta= {1,2,5,7};
		System.out.println("ta "); UtilTab.affiche(ta);
		int[] tb = UtilTab.genere(4);
		System.out.println("tb "); UtilTab.affiche(tb);
		int[] tc = UtilTab.somme(ta,tb);
		System.out.println("tc "); UtilTab.affiche(tc);
	}

}
