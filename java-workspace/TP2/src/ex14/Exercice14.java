package ex14;

import java.util.Scanner;

public class Exercice14 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("heur :");
		Scanner sc = new Scanner(System.in);
		int heur = sc.nextInt();
		System.out.println("minute :");
		int minutes = sc.nextInt();
		
		// methode 1:
		double duree = heur + minutes/60.0;
		System.out.println(duree);
		// methode 2 
		System.out.println(heur + minutes/60.0);
		// l'inverse
		int heur1 = (int) (duree);
		
		int minute1 = (int) ((duree - heur1)*60);
		System.out.println("heur : " + heur1);
		System.out.println("minute : " + minute1);
	}

}
