package ex44;

class Point {
	public Point(int abs, int ord) {
		x = abs;
		y = ord;
	}

	public void affiche() {
		System.out.println("Coordonnees " + x + " " + y);
	}

	private double x; // abscisse
	private double y; // ordonnee
	
	public static Point maxNorme(Point a, Point b){
		double na=Math.sqrt(a.x * a.x + a.y * a.y);
		double nb=Math.sqrt(b.x * b.x + b.y * b.y);
//		return (na>nb)?na:nb;
		if(na>nb) return a;
		else return b;
	}
	
	public Point maxNorme(Point b){
		double na=Math.sqrt(this.x * this.x + this.y * this.y);
		double nb=Math.sqrt(b.x * b.x + b.y * b.y);
//		return (na>nb)?na:nb;
		if(na>nb) return this;
		else return b;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Coordonnees " + x + " " + y;
	}
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Point p1 = new Point(3, 5);
		Point p2 = new Point(4, 6);
		Point p = Point.maxNorme(p1, p2);
		p.affiche();
		System.out.println(p);
		Point p4 = p1.maxNorme(p2);
		p4.affiche();
			
	}

}
