package ex45;

public class Util {
	
	public static int acker(int m, int n){
		if(m<0 || n < 0) return 0;
		else if(m==0) return n+1;
		else if(n==0) return acker(m-1,1);
		else return acker(m-1, acker(m, n-1) );
	}
	
	public static int fact (int n) {
		if(n==0) return 1;
		else return n*fact(n-1);
	}
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		System.out.println(Util.acker(10,3));
		System.out.println(Util.fact(10));

	}

	

}
