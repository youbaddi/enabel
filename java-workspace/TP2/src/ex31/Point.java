package ex31;

public class Point {

    String nom;
    double abscisse;

    public Point(String newNom, double newAbscisse){
        nom=newNom;
        abscisse = newAbscisse;
    }


    public void afficher(){
        System.out.println("point de nom " + nom + " et abscisse " + abscisse);
    }

    public void translate(double dx){
        abscisse +=dx;
    }

}

