package ex31;

import ex18.Clavier;

public class MainApp {

    public static void main(String[] args){
        double x = Clavier.lireDouble();
        Point p = new Point("C",x);
        p.afficher();
        p.translate(2.5);
        p.afficher();
    }

}
