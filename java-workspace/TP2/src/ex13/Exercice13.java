package ex13;

import java.util.Scanner;

public class Exercice13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int degre1 = sc.nextInt();
		int degre2 = sc.nextInt();
		
		// methode 1:
		int min = Math.min(degre1, degre2);
		System.out.println(Math.sin(min));
		// methode 2 
		System.out.println(Math.sin(Math.min(degre1, degre2)));
	}

}
