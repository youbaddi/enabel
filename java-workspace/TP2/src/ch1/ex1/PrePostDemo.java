package ch1.ex1;

public class PrePostDemo {
	public static void main(String[] args) {
		int i = 3;
		i++;
		System.out.println(i);
		++i;
		System.out.println(i);
		System.out.println(++i);
		System.out.println(i++);
		System.out.println(i);
		
		int x=6,y=3,z=2;
		System.out.println((x/y)%(-z) );
		System.out.println(x/y%-z );
	}
}
