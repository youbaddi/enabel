package ma.project.beans;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.hibernate.Session;
import org.hibernate.Transaction;

import ma.project.HibernateUtil;
import ma.project.entities.User;

@ManagedBean
@SessionScoped
public class UserManagedBean implements Serializable{

	private final String SUCCESS = "success";
	private final String ERROR = "error";
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	 private String username;
	 private String password;
	 
	 
	 
	 
	 
	 
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	
	public String save(){
		System.out.println("save in database");
        Session session = HibernateUtil.getSessionFactory().openSession();
        
        Transaction tx = null;
        // start transaction
        try {
        tx = session.beginTransaction();
        User user = new User();
        user.setName(this.name);
        user.setUsername(this.username);

        user.setPassword(this.password);
        
        session.save(user);
        // end transaction
        session.getTransaction().commit();
        return SUCCESS;
        }catch(Exception e) {
        	System.out.println(e.getMessage());
        	return ERROR;
        }
        
        
        
		
	}
	 
	 
}
