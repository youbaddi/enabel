<%@page import="ma.projet.beans.Etudiant"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.mysql.fabric.xmlrpc.base.Array"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

<% ArrayList<Etudiant> etudiants = (ArrayList<Etudiant>)request.getAttribute("etudiants"); %>



<table border="1">
<thead>
     <td>Id</td>
     <td>Nom</td>
     <td>PreNom</td>
     <td>Cin</td>
     <td>Cne</td>
     <td>Actions</td>
     
</thead>

<tbody>

<%for(Etudiant etudiant : etudiants) {%>
 <tr>
     <td><%=etudiant.getId() %></td>
     <td><%=etudiant.getNom() %></td>
     <td><%=etudiant.getPrenom() %></td>
     <td><%=etudiant.getCin() %></td>
     <td><%=etudiant.getCne() %></td>
     <td>  <a href="/etudiantManagment/etudiant/edit?id=<%=etudiant.getId()%>">edit</a> <a href="/etudiantManagment/etudiant/delete?id=<%=etudiant.getId()%>">delete</a></td>
     </tr>
     <%} %>
     
</tbody>

</table>
</body>
</html>