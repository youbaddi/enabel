<%@page import="ma.projet.beans.Etudiant"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.mysql.fabric.xmlrpc.base.Array"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
</head>
<body>

<% ArrayList<Etudiant> etudiants = (ArrayList<Etudiant>)request.getAttribute("etudiants"); %>



<table class="table table-striped table-bordered table-hover">
<thead>
     <td>Num</td>
     <td>Id</td>
     <td>Nom</td>
     <td>PreNom</td>
     <td>Cin</td>
     <td>Cne</td>
     <td>Actions</td>
     
</thead>

<tbody>

<c:forEach   var="etudiant" items="${etudiants}"  varStatus="status" step="1">
 <tr>
    <td><c:out value="${status.index}" /> -<c:out value="${status.count}" /> - <c:out value="${status.first}" /> - <c:out value="${status.last}" /></td>
     <td><c:out value="${etudiant.id}" /> </td>
     <td><c:out value="${etudiant.nom}" escapeXml="false" /> </td>
     <td><c:out value="${etudiant.prenom}" /> </td>
     <td><c:out value="${etudiant.cin}" /> </td>
     <td><c:out value="${etudiant.cne}" /> </td>
     <td>  <a href="/etudiantManagment/etudiant/edit?id=<c:out value="${etudiant.id}" />">edit</a> <a href="/etudiantManagment/etudiant/delete?id=<c:out value="${etudiant.id}" />">delete</a></td>
     </tr>
 </c:forEach>
     
</tbody>

</table>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
</body>
</html>