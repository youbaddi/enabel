package ma.projet.services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ma.projet.IDao.IDao;
import ma.projet.beans.Etudiant;



public class EtudiantService implements IDao<Etudiant> {

	@Override
	public boolean create(Etudiant o) {
		// TODO Auto-generated method stub
		try {
		String query = "INSERT INTO etudiant (nom,prenom,cin,cne) "
				+ "values ('"+o.getNom()+"','"+o.getPrenom()+"','"
				+o.getCin()+"','"+ o.getCne()+"');";
		System.out.println(query);
		Connection cn = Connexion.getConnection();
				System.out.println(cn);
		Statement st = 		cn.createStatement();
		if(st.executeUpdate(query) ==1)
			return true;
		}catch (SQLException e) {
			// TODO: handle exception
			System.out.println("Erreur SQL");
		}
		return false;
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		try {
			String query = "DELETE FROM etudiant where id="+id+";";
			System.out.println(query);
			Statement st = Connexion.getConnection().createStatement();
			if(st.executeUpdate(query) ==1)
				return true;
			}catch (SQLException e) {
				// TODO: handle exception
				System.out.println("Erreur SQL");
			}
			return false;
	}

	@Override
	public boolean update(Etudiant o) {
		// TODO Auto-generated method stub
		try {
			String query = "UPDATE etudiant set nom='"+o.getNom()+
					"', prenom='"+o.getPrenom()+
					"', cin='"+o.getCin()+
					"', cne='"+o.getCne()+
					"' where id="+o.getId()+";";
			System.out.println(query);
			Statement st = Connexion.getConnection().createStatement();
			if(st.executeUpdate(query) ==1)
				return true;
			}catch (SQLException e) {
				// TODO: handle exception
				System.out.println("Erreur SQL");
			}
			return false;
	}

	@Override
	public Etudiant findById(int id) {
		// TODO Auto-generated method stub
		Etudiant result=null;
		try {
			String query = "SELECT * From etudiant where "
					+ "id="+id+";";
			System.out.println(query);
			Statement st = Connexion.getConnection().createStatement();
			ResultSet rs =  st.executeQuery(query);
			if(rs.next())
				result = new Etudiant(rs.getInt("id"),
						rs.getString("nom"),
						rs.getString("prenom"),
						rs.getString("cin"),
						rs.getString("cne"));
				return result;
			}catch (SQLException e) {
				// TODO: handle exception
				System.out.println("Erreur SQL");
			}
			return result;
	}

	@Override
	public List<Etudiant> findAll() {
		// TODO Auto-generated method stub
		List<Etudiant> result= new ArrayList<Etudiant>();
		try {
			String query = "SELECT * From etudiant;";
			System.out.println(query);
			Statement st = Connexion.getConnection().createStatement();
			ResultSet rs =  st.executeQuery(query);
			while(rs.next()) {
				result.add(new Etudiant(rs.getInt("id"),
						rs.getString("nom"),
						rs.getString("prenom"),
						rs.getString("cin"),
						rs.getString("cne")));
			}
			}catch (SQLException e) {
				// TODO: handle exception
				System.out.println("Erreur SQL");
			}
			return result;
	}

}
