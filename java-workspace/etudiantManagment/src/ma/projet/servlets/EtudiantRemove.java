package ma.projet.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ma.projet.beans.Etudiant;
import ma.projet.services.EtudiantService;

/**
 * Servlet implementation class EtudiantIndex
 */

public class EtudiantRemove extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EtudiantRemove() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
	
	
		String id=request.getParameter("id");
		String message ="";
		
		if(id!=null) {
			EtudiantService sc = new EtudiantService();
			
			boolean deleted = sc.delete(Integer.parseInt(id));
			if(deleted) message="Etduaint deleted with success";
			else message="Etduaint delete with fialed; check your connxion";
		}else {
			message="Etduaint deleted with fialed; informations missed";
		}
		
		request.setAttribute("message", message);
		this.getServletContext()
		.getRequestDispatcher("/WEB-INF/etudiantAdd.jsp")
		.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
