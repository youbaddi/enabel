package ma.projet.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ma.projet.beans.Etudiant;
import ma.projet.services.EtudiantService;

/**
 * Servlet implementation class EtudiantIndex
 */

public class EtudiantAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EtudiantAdd() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		
		String nom=request.getParameter("nom");
		String prenom=request.getParameter("prenom");
		String cin=request.getParameter("cin");
		String cne=request.getParameter("cne");
		String message ="";
		
		if(nom!=null &&  prenom!=null && cne!=null && cin!=null) {
			EtudiantService sc = new EtudiantService();
			Etudiant etudiant = new Etudiant(nom, prenom, cin, cne);
			boolean created = sc.create(etudiant);
			if(created) message="Etduaint added with success";
			else message="Etduaint added with fialed; check your connxion";
		}else {
			message="Etduaint added with fialed; informations missed";
		}
		
		request.setAttribute("message", message);
		this.getServletContext()
		.getRequestDispatcher("/WEB-INF/etudiantAdd.jsp")
		.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
