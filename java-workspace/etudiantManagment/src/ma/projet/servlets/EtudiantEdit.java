package ma.projet.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ma.projet.beans.Etudiant;
import ma.projet.services.EtudiantService;

/**
 * Servlet implementation class EtudiantIndex
 */

public class EtudiantEdit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EtudiantEdit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id=request.getParameter("id");
		String nom=request.getParameter("nom");
		String prenom=request.getParameter("prenom");
		String cin=request.getParameter("cin");
		String cne=request.getParameter("cne");
		String message ="";
		
		if(id!=null && nom!=null &&  prenom!=null && cne!=null && cin!=null) {
			EtudiantService sc = new EtudiantService();
			Etudiant etudiant = sc.findById(Integer.parseInt(id));
			etudiant.setNom(nom);
			etudiant.setPrenom(prenom);
			etudiant.setCin(cin);
			etudiant.setCne(cne);
			boolean created = sc.update(etudiant);
			if(created) message="Etduaint updated with success";
			else message="Etduaint updated with fialed; check your connxion";
		}else {
			message="Etduaint updated with fialed; informations missed";
		}
		
		request.setAttribute("message", message);
		this.getServletContext()
		.getRequestDispatcher("/WEB-INF/etudiantAdd.jsp")
		.forward(request, response);
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
