package ma.projet.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ma.projet.beans.Etudiant;
import ma.projet.services.EtudiantService;

/**
 * Servlet implementation class EtudiantIndex
 */

public class EtudiantIndex extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EtudiantIndex() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
	    
		EtudiantService sc = new EtudiantService();
		ArrayList<Etudiant> etudiants = (ArrayList<Etudiant>)sc.findAll();
		
		request.setAttribute("etudiants", etudiants);
		
//		this.getServletContext()
//		.getRequestDispatcher("/WEB-INF/etudiantIndex.jsp")
//		.forward(request, response);
		
		this.getServletContext()
		.getRequestDispatcher("/WEB-INF/etudiantIndexJstl.jsp")
		.forward(request, response);
		
		
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
