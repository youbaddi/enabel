package ma.projet.beans;

public class Etudiant {
	private int id;
	private String nom;
	private String prenom;
	private String cin;
	private String cne;
	public Etudiant(String nom, String prenom, String cin, String cne) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.cin = cin;
		this.cne = cne;
	}
	public Etudiant(int id, String nom, String prenom, String cin, String cne) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.cin = cin;
		this.cne = cne;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getCin() {
		return cin;
	}
	public void setCin(String cin) {
		this.cin = cin;
	}
	public String getCne() {
		return cne;
	}
	public void setCne(String cne) {
		this.cne = cne;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "je suis l'etuciant ayant id = " +id + 
				" le nom " + nom + " prenom " + prenom +
				"cin " + cin + " cne " + cne;
	}
}
