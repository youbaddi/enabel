package com.spring.metier;

import com.spring.data.Client;
import com.spring.interfaces.IClient;
import com.spring.interfaces.IMetier;
import com.spring.produit.Produit;
import com.spring.season.Season;

public class MetierB implements IMetier{

	
private Client client;
private Produit produit;
private Season season;

public Client getClient() {
	return client;
}

public void setClient(Client client) {
	this.client = client;
}


public Produit getProduit() {
	return produit;
}


public void setProduit(Produit produit) {
	this.produit = produit;
}

public Season getSeason() {
	return season;
}

public void setSeason(Season season) {
	this.season = season;
}


@Override
public double calculePrice() {
	// TODO Auto-generated method stub
	System.out.println("calculePrice MetierA");
	double res = 0;
	double pourcentageClient = this.getClient().computePrice();
	double prixProduit = this.getProduit().computePrice();
	double pourcentageSeason = this.getSeason().computePrice();
	double reductionClient   =  prixProduit + (prixProduit *  (pourcentageClient/100));
	res = reductionClient    +(reductionClient * (pourcentageSeason/100));
	//res = res + (res * 2/100);
	return res;
}
}