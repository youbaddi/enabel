package com.spring.season;

public abstract class Season {

	public abstract double computePrice();
}
