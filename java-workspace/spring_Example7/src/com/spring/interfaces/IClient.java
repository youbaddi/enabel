package com.spring.interfaces;

public interface IClient {
   public double computePrice();
}
