package com.spring.interfaces;

public interface ISeason {
   public double computePrice();
}
