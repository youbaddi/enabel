package com.spring.interfaces;

public interface IProduit {
   public double computePrice();
}
