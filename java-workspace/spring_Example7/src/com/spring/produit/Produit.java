package com.spring.produit;

public abstract class Produit {

	public abstract double computePrice();
}
