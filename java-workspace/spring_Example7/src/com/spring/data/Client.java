package com.spring.data;

public abstract class Client {

	public abstract double computePrice();
}
