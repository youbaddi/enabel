package com.spring.presentation;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Method;
import java.util.Properties;
import java.util.Scanner;

import com.spring.data.Client;
import com.spring.produit.Produit;
import com.spring.season.Season;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		// get the calss name from prop file
        
		try {
			FileInputStream f = new FileInputStream("/Volumes/Data/Workspace/formations/formation_benjeloun/enabel/java-workspace/spring_Example7/src/config.proprities");
		
		Properties p = new Properties();
		p.load(f);
		String classClient = p.getProperty("type_client");
        System.out.println(classClient);
        
        String classSeason = p.getProperty("type_season");
        System.out.println(classSeason);
        
        String classProduit = p.getProperty("produit");
        System.out.println(classProduit);
        
        String classMetier = p.getProperty("metier");
        System.out.println(classMetier);
        
        
        
        //
        
           // get class metier   injection d'une depandance
			Class clMetier = Class.forName(classMetier);
			Object oMetier = clMetier.newInstance();
			
			
			
			 // get class client  injection d'une depandance
			Class clClient = Class.forName(classClient);
			Object oClient = clClient.newInstance();
			
			// get class produit  injection d'une depandance
			Class clProduit = Class.forName(classProduit);
			Object oProduit = clProduit.newInstance();
						
						// get class season  injection d'une depandance
			Class clSeason = Class.forName(classSeason );
			Object oSeason = clSeason.newInstance();
			
			
			
			
			Method md1 = clMetier.getMethod("setClient", Client.class);
			md1.invoke(oMetier, oClient);
			
			Method md2 = clMetier.getMethod("setProduit", Produit.class);
			md2.invoke(oMetier, oProduit);
			
			Method md3 = clMetier.getMethod("setSeason", Season.class);
			md3.invoke(oMetier, oSeason);
			
			Method md4 = clMetier.getMethod("calculePrice", null);
			System.out.println(md4.invoke(oMetier, null));
			
			
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
