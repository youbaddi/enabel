package ma.projet.service;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import m.projet.entity.Personne;
import ma.projet.dao.IDao;
import ma.projet.util.HibernateUtil;

public class PersonneService implements IDao<Personne> {
	
	private Session session;
	private Transaction tx;

	@Override
	public boolean create(Personne o) {
		// TODO Auto-generated method stub
		
		try {
		session = HibernateUtil.getSessionFactory().openSession();
		tx = session.beginTransaction();
		session.save(o);
		tx.commit();
		return true;
		}catch (Exception e) {
			// TODO: handle exception
			if(tx!=null) tx.rollback();
		}
				
		return false;
	}

	@Override
	public boolean update(Personne o) {
		// TODO Auto-generated method stub
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			session.update(o);
			tx.commit();
			return true;
			}catch (Exception e) {
				// TODO: handle exception
				if(tx!=null) tx.rollback();
			}
		return false;
	}

	@Override
	public boolean delete(Personne o) {
		// TODO Auto-generated method stub
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			session.delete(o);
			tx.commit();
			return true;
			}catch (Exception e) {
				// TODO: handle exception
				if(tx!=null) tx.rollback();
			}
		return false;
	}

	@Override
	public List<Personne> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Personne geById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
