package ma.projet.bean;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;

import m.projet.entity.Personne;

@ManagedBean
public class PersonnBean implements Serializable{
	
	
	private Personne personne;

	public PersonnBean() {
		personne = new Personne();
	}

	public PersonnBean(Personne personne) {
		this.personne = personne;
	}

	public Personne getPersonne() {
		return personne;
	}

	public void setPersonne(Personne personne) {
		this.personne = personne;
	}
	
	
	
	

}
