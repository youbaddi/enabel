package m.projet.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name="Personne")
public class Personne implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private int id;
	private String nom;
	private String prenom;
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date datNaissence;
	private String ville;
	private String loisir;
	private String sexe;
	
	public Personne() {
		
	}

	public Personne(String nom, String prenom, Date datNaissence, String ville, String loisir, String sexe) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.datNaissence = datNaissence;
		this.ville = ville;
		this.loisir = loisir;
		this.sexe = sexe;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Date getDatNaissence() {
		return datNaissence;
	}

	public void setDatNaissence(Date datNaissence) {
		this.datNaissence = datNaissence;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getLoisir() {
		return loisir;
	}

	public void setLoisir(String loisir) {
		this.loisir = loisir;
	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}
	
	
	
	

}
