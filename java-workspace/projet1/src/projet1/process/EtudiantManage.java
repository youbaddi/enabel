package projet1.process;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import projet1.beans.Etudiant;

/**
 * Servlet implementation class EtudiantManage
 */

public class EtudiantManage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EtudiantManage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
	
		 int id;
		 String nom;
		 String prenom;
		 String cin;
		 String cne;
		 id = Integer.parseInt(request.getParameter("id"));
		 nom = request.getParameter("nom");
		 prenom = request.getParameter("prenom");
		 cin = request.getParameter("cin");
		 cne = request.getParameter("cne");
		 
		 Etudiant bean1 = new Etudiant();
		 bean1.setId(id);
		 bean1.setNom(nom);
		 bean1.setPrenom(prenom);
		 bean1.setCin(cin);
		 bean1.setCne(cne);
		
		request.setAttribute("message", "Etudiant created with succes"); 
		request.setAttribute("Etudiant", bean1);
		
		this.getServletContext()
		.getRequestDispatcher("/WEB-INF/succes.jsp")
		.forward(request, response);
		 
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
