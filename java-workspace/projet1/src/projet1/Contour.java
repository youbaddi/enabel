package projet1;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Contour
 */
@WebServlet("/Contour")
public class Contour extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Contour() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		String param1 = request.getParameter("lang");
		System.out.println(param1);
		
		String message = "this code come from servlet";
		request.setAttribute("message", message);
		
		String message1 = "this code come from servlet";
		request.setAttribute("message1", message1);
		
		request.setAttribute("message2", param1);
		
		//Enumeration<String> attributes = request.getAttributeNames();
//		while (attributes.hasMoreElements()) {
//			String string = (String) attributes.nextElement();
//			System.out.println(string);
//		}
		
		this.getServletContext()
		.getRequestDispatcher("/WEB-INF/test.jsp?lang="+param1)
		.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
