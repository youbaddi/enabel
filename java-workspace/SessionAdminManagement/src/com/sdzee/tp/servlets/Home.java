package com.sdzee.tp.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sdzee.tp.beans.Client;
import com.sdzee.tp.forms.ConnexionForm;

import ma.projet.services.ClientService;

public class Home extends HttpServlet {
    public static final String ATT_USER         = "utilisateur";
    public static final String ATT_FORM         = "form";
    public static final String ATT_SESSION_USER = "sessionUtilisateur";
    public static final String VUE_USER              = "/WEB-INF/user.jsp";
    public static final String VUE_ADMIN              = "/WEB-INF/admin.jsp";
    public static final String HOME              = "/home";
    public static final String LOGIN              = "/WEB-INF/connexion.jsp";

    
    
    public void  sameGetPost(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Affichage de la page de connexion */
        
    	/* Récupération de la session depuis la requête */
        HttpSession session = request.getSession();
    	Client utilisateur = (Client)session.getAttribute( ATT_SESSION_USER);
    	System.out.println(utilisateur.getId());
    	System.out.println(request.getMethod());
    	if(utilisateur ==null) {
    		System.out.println("go to user login");
    		
    		this.getServletContext().getRequestDispatcher( LOGIN ).forward( request, response );
    	}else {
    		System.out.println(utilisateur.getRole());
    		if(utilisateur.getRole().equalsIgnoreCase("admin")) {
    			System.out.println("go to admin home");
    			ClientService cs = new ClientService();
    			List<Client> clients = cs.findAll();
    			request.setAttribute("clients", clients);
        		this.getServletContext().getRequestDispatcher( VUE_ADMIN ).forward( request, response );
    		}else {
    			System.out.println("go to user home");
        		this.getServletContext().getRequestDispatcher( VUE_USER ).forward( request, response );
    		}
    		
    	}
    	
    }
    
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Affichage de la page de connexion */
        
    	/* Récupération de la session depuis la requête */
    	sameGetPost(request, response);
    	
    }
    
    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Affichage de la page de connexion */
        
    	/* Récupération de la session depuis la requête */
    	sameGetPost(request, response);
    	
    }

   
}