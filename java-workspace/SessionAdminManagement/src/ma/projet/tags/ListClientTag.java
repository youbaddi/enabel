package ma.projet.tags;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.DynamicAttributes;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.sdzee.tp.beans.Client;

public class ListClientTag extends SimpleTagSupport implements DynamicAttributes {

	private String items;
	
	
	
	public void setItems(String items) {
		this.items = items;
	}

	@Override
	public void setDynamicAttribute(String uri, String localName, Object value) throws JspException {
		// TODO Auto-generated method stub
		
	}
	
	public void doTag() throws JspException, IOException{
		JspWriter out = getJspContext().getOut();
		
		out.print ("<table border=\"1\">\n" + 
				"<thead>\n" + 
				"     <td>Id</td>\n" + 
				"     <td>Nom</td>\n" + 
				"     <td>PreNom</td>\n" + 
				"     <td>Cin</td>\n" + 
				"     <td>Cne</td>\n" + 
				"\n" + 
				"     \n" + 
				"</thead>\n" + 
				"\n" + 
				"<tbody>\n" );
				
				
				 
			System.out.println("items : " + items);	
				
		if (items!=null) {
			// On recherche un attribut avec le même nom dans les != scopes
			List<Client> clients = (ArrayList<Client>)getJspContext().findAttribute(items);
			//value = o==null ? "" : o.toString();
			for (Client client : clients) {
				out.print (" <tr>\n" + 
						"     <td>"+client.getId()+"</td>\n" + 
						"     <td>"+client.getNom()+"</td>\n" + 
						"     <td>"+client.getPrenom()+"</td>\n" + 
						"     <td>"+client.getEmail()+"</td>\n" + 
						"     <td>"+client.getRole()+"</td>\n" + 
						"      </tr>\n");
			}
		}
				
				out.print ("     \n" + 
				"</tbody>\n" + 
				"\n" + 
				"</table>");
	}

}
