package ma.projet.IDao;

import java.util.List;

public interface IDao<T> {
	
	boolean create(T o);
	boolean delete(int id);
	boolean update(T o);
	List<T> findAll();
    T findById(int id);
}
