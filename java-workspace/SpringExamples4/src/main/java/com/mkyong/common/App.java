package com.mkyong.common;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        AbstractApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");

		HelloWorld obj = (HelloWorld) context.getBean("helloBean");
		System.out.println(obj);
		obj.printHello();
		
		
		HelloWorld obj1 = (HelloWorld) context.getBean("helloBean1");
		obj1.printHello();
		obj1.setAge(39);
		obj1.printHello();
		
		
		
		HelloWorld obj2 = (HelloWorld) context.getBean("helloBean1");
		obj2.printHello();
		
		
		
		context.registerShutdownHook();    // en version 4
		context.close();

    }
}
