import java.lang.reflect.Method;
import java.util.Scanner;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String c = "hello World";
		System.out.println(c);
		System.out.println(c.getClass());
        Class c1 = c.getClass();
        Class c2 = String.class;
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c2.getName());
        System.out.println(c2.getSuperclass());
        System.out.println(c2.getSuperclass().getName());
        
        Scanner sc = new Scanner(System.in);
        String className = sc.next();
        System.out.println(className);
        
        
        try {
			Class cl = Class.forName(className);
			Object o = cl.newInstance();
			
			Method md = cl.getMethod("pub", null);
			md.invoke(o, null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
