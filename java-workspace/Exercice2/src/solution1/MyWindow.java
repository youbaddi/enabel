package solution1;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;

public class MyWindow extends JFrame implements MouseListener {

	private static int nbWindow=0;
	private int num;
	
	public MyWindow() {
		// TODO Auto-generated constructor stub
		nbWindow++;
		this.num = nbWindow;
		setTitle("Window of events num = "+ num);  // setTitle(title);
		setBounds(15,25,350,250);
		addMouseListener(this);
		
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		System.out.println("mousse click on window " + this.num + " on "+ e.getX() + " "+ e.getY());
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
