import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Method;
import java.util.Properties;
import java.util.Scanner;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		// get the calss name from prop file
        FileInputStream f;
		try {
			f = new FileInputStream("src/config.proprities");
		
		Properties p = new Properties();
		p.load(f);
		String className = p.getProperty("class");
        System.out.println(className);
        
        //
        
			Class cl = Class.forName(className);
			Object o = cl.newInstance();
			
			Method md = cl.getMethod("pub", null);
			md.invoke(o, null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
