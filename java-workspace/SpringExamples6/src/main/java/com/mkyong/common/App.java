package com.mkyong.common;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        AbstractApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");

        TextEditor te = (TextEditor) context.getBean("textEditor");
        te.spellCheck();
        System.out.println(te.getName());

    }
}
