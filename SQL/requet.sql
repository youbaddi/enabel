# 1
select * from Poste;
select typePoste from Poste where nPoste='p8';
# 2
select nomLog from Logiciel where typeLog='UNIX';
# 3
SELECT nPoste, nomPoste, indIP, ad, nSalle
from Poste
where typePoste='UNIX' OR typePoste='PCWS';

# 4
SELECT nPoste, nomPoste, indIP, ad, nSalle
from Poste
where (typePoste='UNIX' OR typePoste='PCWS')
AND indIP='130.120.80'
order by nSalle DESC ;

#5
select nlog from Installer where nPoste='p6';

#6
select nPoste  from Installer where nlog='log1';

#7
select nomPoste,CONCAT(indIP,'.', ad) as 'IP' from Poste where typePoste='TX';

#8
select COUNT(nLog)
from Installer;

#9
select nSalle, COUNT(nPoste)
from Poste group by nSalle
order by 2  ;

#10
select nLog, COUNT(nPoste) from Installer
group by nLog;

#11
select AVG(prix) from Logiciel
where typeLog = 'UNIX';

#11 bonus
select typeLog, AVG(prix) from Logiciel
group by typeLog

#12
select MAX(dateAch) from Logiciel;

#13
select count(nlog), nPoste
from Installer
group by nPoste
having count(nlog)=2;

#14
select count(*) from
    (select  nPoste
from Installer
group by nPoste
having count(nlog)=2 ) T;

#15
select typeLP from Types
where typeLP not in (
    select typePoste from Poste
    )
#16
select distinct  typeLog from Logiciel
where typeLog  in (
    select typePoste from Poste
    )


#17
select distinct  typePoste from Poste
where typePoste not in (
    select typeLog from Logiciel
    )

#18
select CONCAT(indIP,'.',ad) from Poste
where nPoste in (
    select nPoste from Installer where nLog='log6'
    )

select nPoste, nLog from Installer where nLog='log6'
select CONCAT(indIP,'.',ad) from Poste where nPoste='p8'
#19
select CONCAT(indIP,'.',ad)
from Poste
where nPoste in (
    select nPoste from Installer
    where nLog = (
        select nLog from Logiciel
        where nomLog = 'Oracle 8'
        )
    )



#20
select nomSegment
from Segment
where indIP in (
    select indIP
    from Poste
    where typePoste='TX' and count()=3
    )

#21
select nomSalle
from Salle
where nSalle IN
      (select nSalle
          from Poste
          where nPoste IN
                (
                    select nPoste
                    from Installer
                    where nLog =
                          (select nLog
                              from Logiciel
                              where nomLog='Oracle 6')
                    ))
#22    like 12
select MAX(dateAch) from Logiciel;

select nomLog, dateAch from Logiciel
where dateAch =
      (select MAX(dateAch) from Logiciel)
#23
select concat(indIP,'.',ad)
from Poste as p, Installer as i
where nLog='log6' AND p.nPoste=i.nPoste

#24
select concat(indIP,'.',ad)
from Poste as p, Installer as i, Logiciel as l
where nomLog='Oracle 8'
  AND p.nPoste=i.nPoste and l.nLog=i.nLog

#25
select s.nomSegment
from Segment as s, Poste as p
where s.indIP= p.indIP
AND p.typePoste='TX'
GROUP BY s.indIP
having count(*)=3


#26

select nomSalle
from Salle as s, Poste as p,
     Installer as i, Logiciel as l
where s.nSalle=p.nSalle
          and p.nPoste=i.nPoste
           and          i.nLog =l.nLog
            and nomLog='Oracle 6'

#27

select nomSegment, nomSalle, CONCAT(p.indIP,'.',p.ad), l.nomLog,
       i.dateIns
from Segment as s, Salle as sa, Poste as p, Logiciel as l, Installer as i
where s.indIP = sa.indIP
AND sa.nSalle =  p.nSalle
and p.nPoste=i.nPoste
and i.nLog=l.nLog
order by nomSegment, nomSalle, CONCAT(p.indIP,'.',p.ad);

select REPLACE(nomSegment,'em','ab') from Segment
where length(nomSegment)>4;
select * from Poste;

select distinct typePoste from Poste
select * from Types