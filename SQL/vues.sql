CREATE VIEW LogicielsUnix
  AS SELECT *
     FROM   Logiciel
     WHERE  typeLog = 'UNIX';

DESCRIBE LogicielsUnix;

SELECT * FROM LogicielsUnix;

CREATE VIEW Poste_0 (
    nPos0, nomPoste0, nSalle0, TypePoste0, indIP, ad0)
AS select nPoste, nomPoste, nSalle, typePoste, indIP, ad
from Poste
where indIP in
      (select indIP
          from Segment
          where etage = 0);

INSERT into Poste_0 values ('p15','B15','s01','UNIX','130.120.80','20');
INSERT into Poste_0 values ('p17','B16','s21','UNIX','130.120.82','20');


delete from Poste where nPoste in ('p15','p16','p17');



CREATE VIEW  SallePrix (
    nSalle, nomSalle, nbPoste, prixLocation)
AS select nSalle,nomSalle,nbPoste,nbPoste*100 from Salle


select * from SallePrix;

select * from SallePrix
where prixLocation>150;

alter table Types ADD tarif SMALLINT(4);

insert into Types values ('BeOS', 'Systeme Be', null);

UPDATE Types set Types.tarif=50 where typeLP='TX'
UPDATE Types set Types.tarif=100 where typeLP='PCWS'
UPDATE Types set Types.tarif=120 where typeLP='PCNT'
UPDATE Types set Types.tarif=200 where typeLP='UNIX'
UPDATE Types set Types.tarif=80 where typeLP='NC'
UPDATE Types set Types.tarif=400 where typeLP='BeOS'


CREATE or replace VIEW SalleIntermediaire(
    nSalle, typePoste, nombre, tarif)
As select p.nSalle, p.typePoste, count(p.nPoste), t.tarif
from Poste as p, Types as t
where p.typePoste = t.typeLP
group by p.nSalle, p.typePoste


CREATE or replace VIEW SallePrixTotal(nSalle, PrixReel)
As select nSalle, SUM(nombre*tarif)
from SalleIntermediaire
group by nSalle


select * from SallePrixTotal
where PrixReel = (
select min(PrixReel) from SallePrixTotal)


#

CREATE or replace VIEW Poste_0 (
    nPos0, nomPoste0, nSalle0, TypePoste0, indIP, ad0)
AS select nPoste, nomPoste, nSalle, typePoste, indIP, ad
from Poste
where indIP in
      (select indIP
          from Segment
          where etage = 0)
with check option
;

select * from Poste_0;
INSERT into Poste_0 values ('p19','B15','s01','UNIX','130.120.82','21');

select * from Poste;