

delimiter $
create procedure procedure2()
begin
DECLARE v_nPost varchar(7);
DECLARE v_nSalle varchar(7);
DECLARE v_nLog  varchar(7);
DECLARE v_numIns  INTEGER(5);
DECLARE v_nomLog  varchar(20);
DECLARE v_dateIns timestamp;

select numIns, nPoste, nLog, dateIns
    from Installer where numIns= (select max(numIns) from Installer);

    select numIns, nPoste, nLog, dateIns
        INTO v_numIns,v_nPost, v_nLog, v_dateIns
    from Installer where numIns= (select max(numIns) from Installer);

    select nSalle into v_nSalle from Poste where nPoste = v_nPost;

    select nomLog into v_nomLog from Logiciel where nlog = v_nLog;

    select CONCAT('Dernier installation en salle: ',v_nSalle) as "Resultat 1 exo 1";
    select CONCAT('Post: ',v_nPost,' Logiciel: ',v_nomLog, 'en date du ', v_dateIns) as "Resultat 2 exo 1";
end $

create procedure procedureInsertPost(v_npost varchar(7), v_nompost vr)
begin
    INSERT INTO Poste VALUES ('p1','Poste 1','130.120.80','01','TX','s01');
end $

delimiter ;

CALL procedure2();
CALL procedureInsertPost('p1','Poste 1','130.120.80','01','TX','s01');



create procedure installLogSeg(IN param1 VARCHAR(11), IN param2 VARCHAR(11),
IN param3 VARCHAR(20), IN param4 timestamp,
    IN param5 VARCHAR(7), IN param6 VARCHAR(9), IN param7 decimal(6,2))
    begin
        set autocommit = 0;

        select nposte from Poste p, Salle s where p.indIP = param1 and p.typePoste = param6

        insert into Logiciel values (param2,param3,param4,param5,param6,param7,0);

        insert into Installer (nposte, nlog, delai)
        values (,param2,TIMESTIMPDIFF(SECOND,param4,SYSDATE)/(24*3600))

    COMMIT
    end;


#TIMESTIMPDIFF(SECOND,param4,SYSDATE)/(24*3600

CALL installLogSeg('130.120.80', 'log99', 'Blaster', '2005-09-05',
    '9.9', 'PCWS', 999.9)$
