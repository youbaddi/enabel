insert into Installer (nPoste, nLog, dateIns, delai)  values ('p2','log6',SYSDATE(),NULL);

insert into Installer (nPoste, nLog, dateIns, delai)  values ('p8','log1',SYSDATE(),NULL);

insert into Installer (nPoste, nLog, dateIns, delai)  values ('p10','log1',SYSDATE(),NULL);

ALter table Segment
ADD (nbSalle tinyint(2) default 0,
    nbPost tinyint(2) default 0);

ALter table Poste
ADD (nbLog tinyint(2) default 0);

ALter table Logiciel
ADD (nbInstall tinyint(2) default 0);



update Segment seg
set seg.nbSalle = (
    select count(*)
    from Salle as s
    where s.indIP = seg.indIP
)

update Segment seg
set seg.nbPost = (
    select count(*)
    from Poste as s
    where s.indIP = seg.indIP
)

select * from Segment;
select count(*) from Salle where indIP='130.120.80';

update Logiciel l
set l.nbInstall = (
    select count(*)
    from Installer as i
    where i.nLog = l.nLog
)

select * from Logiciel

update Poste p
set p.nbLog = (
    select count(*)
    from Installer as i
    where p.nPoste = i.nPoste
)

select * from Poste