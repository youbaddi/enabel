CREATE TABLE Softs AS SELECT nomLog, version, prix from Logiciel;

select * from Softs;

ALTER TABLE Softs CHANGE nomLog nomSoft varchar(20);

CREATE TABLE PCSeuls
    AS SELECT nPoste, nomPoste, indIP, ad, typePoste, nSalle from Poste
where typePoste='PCNT' OR typePoste='PCWS';

select * from PCSeuls;

ALTER TABLE PCSeuls CHANGE nPoste np VARCHAR(7);

ALTER TABLE PCSeuls CHANGE nomPoste nomP VARCHAR(20);

ALTER TABLE PCSeuls CHANGE IndIP seg VARCHAR(11);

ALTER TABLE PCSeuls CHANGE typePoste typeP VARCHAR(9);

ALTER TABLE PCSeuls CHANGE lieu salle VARCHAR(9);
